<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Editar Perfil de Cliente<span class="title-under"></span></h1>
			<p class="page-description">Bebemos para que los demás parezcan más interesantes</p>
		</div>
	</div>

<!-- <h4 style="text-align: center;"><b>Editar Cliente</b><span class="title-under"></span></h4> -->
<div class="container-fluid">
<form method="POST" action="personas-edicion-back.php">
    <div class="col-md-4 col-sm-12 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Nombre:</label>
            <input id="nombre" name="nombre" type="text" class="form-control" />
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Primer Apellido:</label>
            <input id="apellido1" name="apellido1" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Segundo Apellido:</label>
            <input id="apellido2" name="apellido2" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Cédula:</label>
            <input id="cedula" name="cedula" type="text" maxlength="10" class="form-control"/>
        </fieldset>
    </div>
    <div class="col-md-4 col-sm-12 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Provincia:</label>
            <select name='provincia' id="provincia" class="form-control">>
                <option value="none" >Seleccione una provincia</option>
                <?php
                    $db = new PDO(HOST,USER,PASS);
                    $t = $db->prepare('call getProvincia();');
                    $t->execute();
                    //$t->setFetchMode(PDO::FETCH_ASSOC);
                    foreach($t as $provincia){
                        echo '<option  value="'.$provincia['id'].'">'.$provincia['descripcion'].'</option>';
                    }
                ?>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Cantón:</label>
            <select name='canton' id="canton" class="form-control">>
                <option value="none" >Seleccione un cantón</option>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Distrito:</label>
            <select name='distrito' id="distrito" class="form-control">>
                <option value="none" >Seleccione un distrito</option>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Otra Dirección:</label>
            <textarea id="direccion" name="direccion" rows="5" cols="40" class="form-control"></textarea>
        </fieldset>
    </div>
    <div class="col-md-4 col-sm-12 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Contraseña:</label>
            <input id="password" name="password" type="password" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Repetir Contraseña:</label>
            <input id="REpassword" name="REpassword" type="password" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit" value="Guardar" class="btn btn-primary btn-block"/>
        </fieldset>
    </div>
</form>
</div>

<?php
if(!empty($_COOKIE['empleado'])||!empty($_COOKIE['cliente'])){
    //echo '<script>alert("'.$_COOKIE['user'].'");</script>';
    $user;
    if (!empty($_COOKIE['cliente'])){
            $user='cliente';
        }
        if (!empty($_COOKIE['empleado'])){
            $user='empleado';
        }
    $db = new PDO(HOST,USER,PASS);
    $result = $db->prepare('call getPersona(:nombreUsuario);');
    $result->execute(array(':nombreUsuario'=>$_COOKIE[$user]));
    $idDistrito;
    $idCanton;
    $idProvincia;
    foreach($result as $i){?>
    <script>document.getElementById("nombre").value = <?php echo '"'.$i['nombre'].'"';?> ;</script>
    <script>document.getElementById("apellido1").value = <?php echo '"'.$i['primerApellido'].'"';?> ;</script>
    <script>document.getElementById("apellido2").value = <?php echo '"'.$i['segundoApellido'].'"';?> ;</script>
    <script>document.getElementById("cedula").value = <?php echo '"'.$i['numeroCedula'].'"';?> ;</script>
    <script>document.getElementById("direccion").value = <?php echo '"'.$i['direccionExacta'].'"';?> ;</script>


    <?php
    $idDistrito = $i['idDistrito'];
    }
    $result = $db->prepare('call getDistrito()');
    $result->execute();
    foreach($result as $distrito){
        if ($idDistrito == $distrito['id']){
            $idCanton = $distrito['idCanton'];}
    }
    $result = $db->prepare('call getDistrito()');
    $result->execute();
    foreach($result as $distrito){?>
            <script>

                var select = document.getElementById("distrito");

                if (<?php echo $idCanton.'=='.$distrito['idCanton'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$distrito["descripcion"]."'";?>;
				    o.value = <?php echo $distrito["id"];?>;
				    select.options.add (o);}


            </script>
                <?php }
            $result = $db->prepare('call getDistrito()');
            $result->execute();
            foreach($result as $distrito){?>
            <script>
                var objSelect = document.getElementById("distrito");
                setSelectedValue(objSelect, <?php if($idDistrito==$distrito['id']){echo '"'.$distrito['descripcion'].'"';}?>);

            //Set selected

                function setSelectedValue(selectObj, valueToSet) {
                    for (var i = 0; i < selectObj.options.length; i++) {
                        if (selectObj.options[i].text== valueToSet) {
                            selectObj.options[i].selected = true;
                            return;
                        }
                    }
                }
            </script>
        <?php
        }
            $result = $db->prepare('call getCanton()');
            $result->execute();
            foreach($result as $canton){
                if ($idCanton == $canton['id']){
                    $idProvincia = $canton['idProvincia'];}
            }
            $result = $db->prepare('call getCanton()');
            $result->execute();
            foreach($result as $canton){?>
            <script>

                var select = document.getElementById("canton");

                if (<?php echo $idProvincia.'=='.$canton['idProvincia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

            }
            </script>
                <?php }


            $result = $db->prepare('call getCanton()');
            $result->execute();
            foreach($result as $canton){?>
            <script>
                var objSelect = document.getElementById("canton");
                setSelectedValue(objSelect, <?php if($idCanton==$canton['id']){echo '"'.$canton['descripcion'].'"';}?>);

            //Set selected

                function setSelectedValue(selectObj, valueToSet) {
                    for (var i = 0; i < selectObj.options.length; i++) {
                        if (selectObj.options[i].text== valueToSet) {
                            selectObj.options[i].selected = true;
                            return;
                        }
                    }
                }
            </script><?php

        }
        $result = $db->prepare('call getProvincia()');
            $result->execute();
            foreach($result as $provincia){?>
            <script>
                var objSelect = document.getElementById("provincia");
                setSelectedValue(objSelect, <?php if($idProvincia==$provincia['id']){echo '"'.$provincia['descripcion'].'"';}?>);

            //Set selected

                function setSelectedValue(selectObj, valueToSet) {
                    for (var i = 0; i < selectObj.options.length; i++) {
                        if (selectObj.options[i].text== valueToSet) {
                            selectObj.options[i].selected = true;
                            return;
                        }
                    }
                }
            </script><?php

        }


        }?>

<!--  Scripts
================================================== -->

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>

<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>

<!-- Template main javascript -->
<script src="assets/js/main.js"></script>
<script>
    $(document).ready(function () {
  //called when key is pressed in textbox
        $("#cedula").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });

    $('#provincia').change(function(){
        var listProvincias = document.getElementById("provincia")
        var provincia = listProvincias.options[listProvincias.selectedIndex].value;
        var select = document.getElementById("canton");
        var o = document.createElement("OPTION");
        o.text="Seleccione un cantón";
        o.value="none";
        select.length = 0;
        select.options.add(o);
        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getCanton();');
            $t->execute();
            foreach($t as $canton){?>
                if (provincia == <?php echo $canton['idProvincia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
        var select2 = document.getElementById("distrito");
        var d = document.createElement("OPTION");
        d.text="Seleccione un distrito";
        d.value="none";
        select2.length = 0;
        select2.options.add(d);
    });

    $('#canton').change(function(){
        var listCantones = document.getElementById("canton")
        var canton = listCantones.options[listCantones.selectedIndex].value;
        var select = document.getElementById("distrito");
        var o = document.createElement("OPTION");
        o.text="Seleccione un distrito";
        o.value="none";
        select.length = 0;
        select.options.add(o);
        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getDistrito();');
            $t->execute();
            foreach($t as $canton){?>
                if (canton == <?php echo $canton['idCanton'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
    });



        </script>
    </body>
</html>
