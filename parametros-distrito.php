<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">CATALOGO <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>





<div class="container-fluid">
<h2 class="title-style-2"><b>Agregar Distrito</b><span class="title-under"></span></h2>



<form action="parametros-distrito.php" method="get">
<fieldset class="form-group">
                <label style="display: block;" >Provincia:</label>
                <select name="provincia" id="provincia" >
                    <option value="none" >Seleccione una provincia</option>
                    <?php
                        $db = new PDO(HOST,USER,PASS);
                        $t = $db->prepare("call getProvincia();");
                        $t->execute();
                        //$t->setFetchMode(PDO::FETCH_ASSOC);
                        foreach($t as $provincia){
                            echo '<option value="'.$provincia['id'].'">'.$provincia['descripcion'].'</option>';
                        }
                    ?>
                </select>
            </fieldset>
    <fieldset class="form-group">
    <label style="display: block;" >Cantón:</label>
                <select name='canton' id="canton" style="width: 200px; display: block; float: left;">
                    <option value="none" >Seleccione un cantón</option>
                </select>
    </fieldset>


    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
        <label style="width: 200px; display: block; float: left;" >Nombre del distrito:</label>
            <input name="distrito" id="distrito" class="form-control">
        </fieldset>
        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit"  value ="Registrar Distrito" />
        </fieldset>
    </div>
</form>
</div>
<?php


        if(isset($_GET['registrar'])&&$_GET['canton']!="none"&&$_GET['distrito']!=''){
            $db = new PDO(HOST,USER,PASS);
            $result = $db ->prepare("call agregardistrito(:canton,:distrito);");
            $result -> execute(array(':canton'=>$_GET['canton'],':distrito'=>$_GET['distrito']));
            echo '<script>alert("Distrito Registrado!");</script>';
        }


        ?>




    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#provincia').change(function(){
        var listProvincias = document.getElementById("provincia")
        var provincia = listProvincias.options[listProvincias.selectedIndex].value;
        var select = document.getElementById("canton");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");
        o.text="Seleccione un cantón";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            //$db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getCanton();');
            $t->execute();
            foreach($t as $canton){?>
                if (provincia == <?php echo $canton['idProvincia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
        var select2 = document.getElementById("distrito");
        var d = document.createElement("OPTION");
        d.text="Seleccione un distrito";
        d.value="none";
        select2.length = 0;
        select2.options.add(d);
    });
</script>

    </body>
</html>
