<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Edición de Perfil de Clientes<span class="title-under"></span></h1>
			<p class="page-description">A través de las tormentas del tiempo,<br>una cerveza siempre es bienvenida!</p>
		</div>
	</div>

<!-- Formulario de búsqueda de clientes por editar> -->

        <!-- Formulario de búsqueda de clientes por editar> -->

<h2 class="title-style-1"><b>Buscar Cliente por Editar</b><span class="title-under"></span></h4>
<div class="container-fluid">
<form action="cliente-editar.php" method="get" id="usuarioslista">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
            <label style="display: block;" >Nombre:</label><br>
            <input id="bnombre" name="bnombre" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Primer Apellido:</label><br>
            <input id="bapellido1" name="bapellido1" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Segundo Apellido:</label><br>
            <input id="bapellido2" name="bapellido2" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Cédula:</label><br>
            <input id="bcedula" name="bcedula" type="text"maxlength="10" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <input id="buscar" name="buscar" type="submit" value="Buscar" class="btn btn-primary btn-block"/>
        </fieldset>
    </div>
   <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <div class="panel-primary">
            <div class="panel-heading"><h5 style="display: block;" >Lista de clientes (Usuario)</h5></div>
            <div class="panel-body" style="min-height:375px;max-height:375px;overflow-y:scroll;">
                <table data-toggle="table" id="table-style" data-row-style="rowStyle"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-single-select="true" data-click-to-select="true" data-maintain-selected="true" class="table table-striped table-hover table-responsive">
                  <thead>
                    <tr>
                      <th>Usuario</th>
                      <th>Nombre</th>
                      <th>Primer Apellido</th>
                      <th>Segundo Apellido</th>
                      <th>Cédula</th>
                    </tr>
                  </thead>
                    <tbody>
                        <?php
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getClientes();');
                $result->execute();
                foreach($result as $i){
                    echo '
                        <tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&!empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getNombrelike(:nombre);');
                $result->execute(array(':nombre'=>$_GET['bnombre']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&!empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getPrimerApellidolike(:apellido1);');
                $result->execute(array(':apellido1'=>$_GET['bapellido1']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&!empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getSegundoApellidolike(:apellido2);');
                $result->execute(array(':apellido2'=>$_GET['bapellido2']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&!empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getCedulalike(:cedula);');
                $result->execute(array(':cedula'=>$_GET['bcedula']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }?>



                    </tbody>
                </table>
            </div>

        </div>
    </div>
</form>
</div>

<h4 style="text-align: center;"><b>Editar Cliente</b><span class="title-under"></span></h4>
<div class="container-fluid">
    <form method="post" action="cliente-editar-back.php">
        <div class="col-md-4 col-sm-6 fadeIn animated col-form">
            <fieldset class="form-group">
                <label style="display: block;" >Nombre:</label>
                <input id="nombre" name="nombre" type="text" class="form-control"/>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Primer Apellido:</label>
                <input id="apellido1" name="apellido1" type="text" class="form-control"/>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Segundo Apellido:</label>
                <input id="apellido2" name="apellido2" type="text" class="form-control" />
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Cédula:</label>
                <input id="cedula" name="cedula" type="text" maxlength="10" class="form-control"/>
            </fieldset>
        </div>
        <div class="col-md-4 col-sm-6 fadeIn animated col-form">

                <fieldset class="form-group">
                    <label style="display: block;" >Provincia:</label>
                    <select name='provincia' id="provincia" class="form-control">
                        <option value="none" >Seleccione una provincia</option>
                        <?php
                            $db = new PDO(HOST,USER,PASS);
                            $t = $db->prepare('call getProvincia();');
                            $t->execute();
                            //$t->setFetchMode(PDO::FETCH_ASSOC);
                            foreach($t as $provincia){
                                echo '<option value="'.$provincia['id'].'">'.$provincia['descripcion'].'</option>';
                            }
                        ?>
                    </select>
                </fieldset>



            <fieldset class="form-group">
                <label style="display: block;" >Cantón:</label>
                <select name='canton' id="canton" class="form-control">
                    <option value="none" >Seleccione un cantón</option>
                </select>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Distrito:</label>
                <select name='distrito' id="distrito" class="form-control">
                    <option value="none" >Seleccione un distrito</option>
                </select>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Otra Dirección:</label>
                <textarea id="direccion" name="direccion" class="form-control"></textarea><!--rows="5" cols="40"-->
            </fieldset>
        </div>
        <div class="col-md-4 col-sm-12 fadeIn animated col-form">
            <fieldset class="form-group">
                <label style="display: block;" >Usuario:</label>
                <input id="user" name="user" type="text" class="form-control"/>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Contraseña:</label>
                <input id="password" name="password" type="password" class="form-control"/>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block;" >Repetir Contraseña:</label>
                <input id="REpassword" name="REpassword" type="password" class="form-control"/>

            </fieldset>
            <fieldset class="form-group">
                <input id="registrar" name="registrar" type="submit" value="Guardar" class="btn btn-primary btn-block"/>
            </fieldset>
        </div>
    </form>
</div>

<!--  Scripts
================================================== -->
<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Template main javascript -->
<script src="assets/js/main.js"></script>
<script>
    $("input[type='radio'][name='listaUsuarios']").click(function(){
        var selected = $("input[type='radio'][name='listaUsuarios']:checked");

        var id = selected.val();
        var select;
        var o;
        var objSelected;
        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getPersonaById();');
            $t->execute();
            $idDistrito;
            $idCanton;
            $idProvincia;
            foreach($t as $result){?>
                if(id==<?php echo $result['id'];?>){
                    document.getElementById("nombre").value = <?php echo '"'.$result['nombre'].'"';?> ;
                    document.getElementById("apellido1").value = <?php echo '"'.$result['primerApellido'].'"';?> ;
                    document.getElementById("apellido2").value = <?php echo '"'.$result['segundoApellido'].'"';?> ;
                    document.getElementById("cedula").value = <?php echo '"'.$result['numeroCedula'].'"';?> ;
                    document.getElementById("direccion").value = <?php echo '"'.$result['direccionExacta'].'"';?> ;
                    document.getElementById("user").value = <?php echo '"'.$result['usuario'].'"';?> ;

                }
            <?php $idDistrito=$result['idDistrito'];
            } // Cierre del foreach ?>
    }); </script> <!-- Cierre del change -->
<script>
    $(document).ready(function () {
    // Called when key is pressed in textbox
        $("#cedula").keypress(function (e) {
        // If the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });

    $(document).ready(function () {
  //called when key is pressed in textbox
        $("#bcedula").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });

    $('#provincia').change(function(){
        var listProvincias = document.getElementById("provincia")
        var provincia = listProvincias.options[listProvincias.selectedIndex].value;
        var select = document.getElementById("canton");
        var o = document.createElement("OPTION");
        o.text="Seleccione un cantón";
        o.value="none";
        select.length = 0;
        select.options.add(o);
        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getCanton();');
            $t->execute();
            foreach($t as $canton){?>
                if (provincia == <?php echo $canton['idProvincia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
        var select2 = document.getElementById("distrito");
        var d = document.createElement("OPTION");
        d.text="Seleccione un distrito";
        d.value="none";
        select2.length = 0;
        select2.options.add(d);
    });

    $('#canton').change(function(){
        var listCantones = document.getElementById("canton")
        var canton = listCantones.options[listCantones.selectedIndex].value;
        var select = document.getElementById("distrito");
        var o = document.createElement("OPTION");
        o.text="Seleccione un distrito";
        o.value="none";
        select.length = 0;
        select.options.add(o);
        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getDistrito();');
            $t->execute();
            foreach($t as $canton){?>
                if (canton == <?php echo $canton['idCanton'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
    });



        </script>
    </body>
</html>
