<?php include("private.php");?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Gallery | Charity / Non-profit responsive Bootstrap HTML5 template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet' type='text/css'>

        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">


        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="assets/css/prettyPhoto.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>


    </head>
    <body>
    <!-- NAVBAR
    ================================================== -->

    <header class="main-header">


        <nav class="navbar navbar-static-top">

            <div class="navbar-top">

              <div class="container">
                  <div class="row">

                    <div class="col-sm-6 col-xs-12">

                        <?php
                            if(empty($_COOKIE['user'])){
                            echo '<Form  method="GET" action="index.php" id="form1">

                            <input type="text" id ="user" name="user" placeholder="Usuario"  style="margin-left:10px;color:#000000";/>

                            <input type="password" id ="password" name="password" placeholder="Contraseña"  style="color:#000000;"/>
                            <input type="submit" id="enter" name ="enter" value="Entrar" style="color:#000000;"/>

                            <a href="registro.php">Registrarse</a>
                            </Form>';

                            if(!empty($_GET['enter'])){
                                if(!empty($_GET['user'])&&!empty($_GET['password'])){
                                $db = new PDO(HOST,USER,PASS);
                                $t = $db->prepare('call getUsuario(:user);');

                                $t->execute(array(':user'=>$_GET['user']));
                                $row = $t -> rowCount();
                                if ($row>0){
                                    foreach($t as $user){

                                        if($user['contrasena']==$_GET['password']){
                                            setcookie("user",$_GET['user'],time()+(86400*7));
                                            echo '<script>location="index.php";</script>';}
                                    }
                                        echo '<script>alert("Contraseña incorrecta");location="index.php";</script>';

                                }else{echo '<script>alert("Usuario no existe");location="index.php";</script>';}




                                /*while($r=$stmt->fetch_assoc()){
                                    echo '<script>alert("'.$r['usuario'].'");</script>';
                                }*/

                                }else{ echo '<script>alert("Usuario o Contraseña invalida");location="index.php";</script>';}
                            }

                            }else{

                                header('Location: http://localhost/Sada/sadaka/HTML/index.php');}

                            ?>
                       </ul> <!-- /.header-social  -->

                    </div>


                  </div>
              </div>

            </div>

            <div class="navbar-main">

              <div class="container">

                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>

                  </button>

                  <a class="navbar-brand" href="index.html"><img src="assets/images/sadaka-logo.png" alt=""></a>

                </div>

                <div id="navbar" class="navbar-collapse collapse pull-right">

                  <ul class="nav navbar-nav">

                    <li><a href="index.php">Inicio</a></li>
                    <li><a href="about.html">ABOUT</a></li>
                    <li class="has-child"><a href="#">CAUSES</a>

                      <ul class="submenu">
                         <li class="submenu-item"><a href="causes.html">Causes list </a></li>
                         <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                         <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                         <li class="submenu-item"><a href="causes-single.html">Single cause </a></li>
                      </ul>

                    </li>
                    <li><a href="gallery.html">GALLERY</a></li>
                    <li><a href="contact.html">CONTACT</a></li>

                  </ul>

                </div> <!-- /#navbar -->

              </div> <!-- /.container -->

            </div> <!-- /.navbar-main -->


        </nav>

    </header> <!-- /. main-header -->


	<div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">CATALOGO <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>

<Form method="POST" action="regis.php">

    <div class="col-md-7 col-sm-12 col-form">
        <h2 class="title-style-2"><b>Registro</b><span class="title-under"></span></h2>
            <table style="float: left;" >
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Nombre:</label>
                        <input id="nombre" name="nombre" type="text" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Primer Apellido:</label>
                        <input id="apellido1" name="apellido1" type="text" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
                <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Segundo Apellido:</label>
                        <input id="apellido2" name="apellido2" type="text" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Cédula:</label>
                        <input id="cedula" name="cedula" type="text" maxlength="10" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
        </table>

        <table style="float: left;">

            <tr><td>&nbsp; </td></tr>
        <tr>
            <td width="500">
                <label style="width: 200px; display: block; float: left;" >Provincia:</label>
                <select name='provincia' id="provincia" style="width: 200px; display: block; float: left;">
                    <option value="none" >Seleccione una provincia</option>
                </select>
            </td>
        </tr>
        <tr><td>&nbsp; </td></tr>
        <tr>
            <td width="500">
                <label style="width: 200px; display: block; float: left;" >Cantón:</label>
                <select name='canton' id="canton" style="width: 200px; display: block; float: left;">
                    <option value="none" >Seleccione un cantón</option>
                </select>
            </td>
        </tr>
        <tr><td>&nbsp; </td></tr>
        <tr>
            <td width="500">
                <label style="width: 200px; display: block; float: left;" >Distrito:</label>
                <select name='distrito' id="distrito" style="width: 200px; display: block; float: left;">
                    <option value="none" >Seleccione un distrito</option>
                </select>
            </td>
        </tr>
            <td>&nbsp; </td></tr>
        <tr>
            <td width="500">
            <label style="width: 200px; display: block; float: left;" >Otra Direccion:</label>
                <textarea id="direccion" name="direccion" rows="5" cols="40" style="margin-bottom:85px;width:400px; height:100px;"></textarea>
            </td>
        </tr>
        </table>

    <table style="float: center;" >
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Usuario:</label>
                        <input id="user" name="user" type="text" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Contraseña:</label>
                        <input id="password" name="password" type="password" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
                  <tr><td>&nbsp; </td></tr>
                  <tr>
                    <td width="500">
                        <label style="width: 200px; display: block; float: left;" >Repetir Contraseña:</label>
                        <input id="REpassword" name="REpassword" type="password" style="width: 200px; display: block; float: left;" />
                    </td>
                  </tr>
        </table>
            <input id="registrar" name="registrar" type="submit" value="Registrase" style="margin-top:100px;width: 200px; display: block; float: center;" />

        </div>
</Form>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>


    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    <script>
    $(document).ready(function () {
  //called when key is pressed in textbox
        $("#cedula").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message

               return false;
    }
   });
});</script>
    </body>
</html>
