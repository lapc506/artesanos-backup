<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">CATALOGO <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>





<div class="container-fluid">
<h2 class="title-style-2"><b>Agregar Familia de Cerveza</b><span class="title-under"></span></h2>
<form action="parametros-familia.php" method="get">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
        <label style="width: 200px; display: block; float: left;" >Nombre de la familia:</label>
            <input name="familia" id="familia" class="form-control">
        </fieldset>

        <fieldset class="form-group">
                <label style="display: block;" >Tipo de Fermentacion:</label>
                <select name="ferm" id="ferm" >
                    <option value="none" >Seleccione un tipo</option>
                    <?php
                        $db = new PDO(HOST,USER,PASS);
                        $t = $db->prepare("call getTipoFerm();");
                        $t->execute();
                        //$t->setFetchMode(PDO::FETCH_ASSOC);
                        foreach($t as $ferm){
                            echo '<option value="'.$ferm['id'].'">'.$ferm['descripcion'].'</option>';
                        }
                    ?>
                </select>
            </fieldset>

        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit"  value ="Registrar familia" />
        </fieldset>

    </div>
</form>
</div>
<?php


        if(isset($_GET['registrar'])){
            if($_GET['familia']!=""&&$_GET['ferm']!="none"){
            $db = new PDO(HOST,USER,PASS);
            $result = $db ->prepare("call agregarFamilia(:familia,:tipoferm);");
            $result -> execute(array(':familia'=>$_GET['familia'],':tipoferm'=>$_GET['ferm']));
            echo '<script>alert("Familia Registrada!");</script>';}else{ echo '<script>alert("Faltan Datos");</script>';}
        }


        ?>




    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>


    </body>
</html>
