<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Ranking de Clientes Frecuentes<span class="title-under"></span></h1>
			<p class="page-description">Sus clientes ahora podrán<br>recibir bonificaciones a futuro</p>
		</div>
	</div>
<div class="col-md-3 col-sm-3 col-xs-12 fadeIn animated col-form">
    <form action="ranking-clientes.php" method="post">
        <h2 class="title-style-2"><b>Defina un límite</b><span class="title-under"></span></h2>
        <fieldset class="form-group">
            <input id="topCliente" name="topCliente" min="1" type="number" class="form-control" style="display: inline;"/>
            <input id="buscar" name="buscar" value="Buscar" type="submit" class="btn btn-primary btn-block"/>
        </fieldset>
    </form>
</div>
<div class="col-md-9 col-sm-6 col-xs-12 fadeIn animated">
    <div class="panel-primary">
        <div class="panel-heading"><h5 style="display: block;" >Top de Clientes Frecuentes:</h5></div>
        <div class="panel-body" style="overflow-y:scroll;">
            <table data-toggle="table" id="table-style"
                   class="table table-striped table-hover table-responsive">
              <thead>
                <tr>
                  <th>Usuario</th>
                  <th>Nombre</th>
                  <th>Primer Apellido</th>
                  <th>Segundo Apellido</th>
                  <th>Total de Cervezas Compradas</th>
                </tr>
              </thead>
                <tbody>
                    <?php
                        if(!empty($_POST["buscar"])){
                            if(empty($_POST["topCliente"])) {
                                echo '<script>alert("Favor ingresar un límite de búsqueda.");</script>';
                            } else {
                                $db = new PDO(HOST,USER,PASS);
                                $result = $db->prepare('call topClientes(:top);');
                                $result->execute(array(':top'=>$_POST['topCliente']));
                                foreach($result as $i){
                                    echo '
                                        <tr>
                                            <td>'.$i['usuario'].'</td>
                                            <td>'.$i['nombre'].'</td>
                                            <td>'.$i['primerApellido'].'</td>
                                            <td>'.$i['segundoApellido'].'</td>
                                            <td>'.$i['cantidadCervezas'].'</td>
                                        </tr>';
                                }
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    </body>
</html>
