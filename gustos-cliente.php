<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">Gustos del cliente <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>
<div class="container-fluid">

    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <div id="piechart"></div>
    </div>
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <div class="panel-primary">
            <div class="panel-heading"><h5 style="display: block;" >Estado de Cuenta</h5></div>
            <div class="panel-body" style="min-height:200px;max-height:200px;overflow-y:scroll;">
                <table data-toggle="table" id="table-style" data-row-style="rowStyle"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-single-select="true" data-click-to-select="true" data-maintain-selected="true" class="table table-striped table-hover table-responsive">
                  <thead>
                    <tr>
                      <th>Cerveza</th>
                      <th>Cantidad</th>
                      <th>Fecha de compra</th>
                    </tr>
                  </thead>
                    <tbody>
                        <?php

                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getComprasPorNombre(:nombre);');
                $result->execute(array(':nombre'=>$_COOKIE['cliente']));

                foreach($result as $i){
                    echo '
                        <tr>

                            <td>'.$i['marca'].'</td>
                            <td>'.$i['cantidad'].'</td>
                            <td>'.$i['fecha'].'</td>
                        </tr>';
                }


            ?>



                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Piechart -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Marca', 'Cantidad'],
            <?php
                    $db = new PDO(HOST,USER,PASS);
                    $result = $db->prepare('call getComprasPorNombreBack(:nombre);');
                    $result->execute(array(':nombre'=>$_COOKIE['cliente']));
                    $cant=0;
                    foreach($result as $i){
                        echo '["'.$i['marca'].'",'.$i['suma'].']';
                        ++$cant;
                        if($cant < $result ->rowCount()){
                            echo ',';
                        }
                    }
            ?>
        ]);

        var options = {
          title: 'Gustos'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>

    </body>
</html>
