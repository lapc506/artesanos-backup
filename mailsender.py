import smtplib
from email.mime.text import MIMEText
import mysql.connector as mariadb

def sendmail():
    try:
        cnx = mariadb.connect(user='demo', password='demo',
            host='127.0.0.1',database='artesanos')
        correos = cnx.cursor()
        correos.execute("SELECT * FROM correos WHERE "+
                        "DATE(created) = DATE(NOW()) AND "+
                        "sent = false")
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo();  server.starttls()
        subject  = 'Notificacion de nuevas cervezas'
        fromaddr = 'artesanosdb2016@gmail.com'
        toaddr   = 'lapc20081996@gmail.com'
        password = 'proyecto2'
        server.login(fromaddr,password)
        idsEnviados = []
        for (id, msg, date, sent) in correos:
            rows = msg[:-1].split(";")
            extended_msg_as_table = ""
            for row in rows:
                for field in row.split(","):
                    extended_msg_as_table += ('{:30}'.format(field))
                extended_msg_as_table += "\n"
            # "From: "+fromaddr,"To: "+toaddr,"Subject: "+subject,"",
            mailmsg = "\r\n".join(["Estimado administrador,","","",
                "le comunicamos que estas son las nuevas cervezas registradas hoy:","","",
                extended_msg_as_table, "", "",
                "Agradecemos su preferencia.\nDe parte del equipo de ArtesanosDB"])
            # Conversión de Unicode para enviar un MIMEText y no un correo ASCII:
            finalmsg = MIMEText(mailmsg.encode('utf-8'), _charset='utf-8')
            finalmsg['Subject'] = subject
            finalmsg['From'] = fromaddr
            finalmsg['To'] = toaddr
            server.sendmail(fromaddr, [toaddr], finalmsg.as_string())
            print(finalmsg)
            idsEnviados.append(id)
        server.quit()
        print("Envió correctamente: \n")
        for ids in idsEnviados:
            print(ids)
            #correos.execute("UPDATE correos SET sent=true WHERE id=%s"%(ids))
        correos.close()
        cnx.close()
    except BaseException as errmsg:
        print('Something went wrong... '+str(errmsg))

sendmail()

