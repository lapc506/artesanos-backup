<?php
include ("private.php");
if(!empty($_POST['registrar'])
   &&!empty($_POST['nombre'])
   &&!empty($_POST['apellido1'])
   &&!empty($_POST['apellido2'])
   &&!empty($_POST['cedula'])
   &&!empty($_POST['direccion'])
   &&!empty($_POST['password'])
   &&$_POST['provincia']!='none'
   &&$_POST['canton']!='none'
   &&$_POST['distrito']!='none')
{
    if($_POST['password']==$_POST['REpassword'])
    {
        $db = new PDO(HOST,USER,PASS);
        $user;
        if (!empty($_COOKIE['cliente'])){
            $user='cliente';
        }
        if (!empty($_COOKIE['empleado'])){
            $user='empleado';
        }
        $stmt = $db ->prepare("call modificarUsuario(:nombre,:primerApellido,".
                              ":segundoApellido,:cedula,:idDistrito,".
                              ":direccion,:usuario,SHA1(:contrasena));");
        $stmt -> execute(array(':nombre'=> $_POST['nombre'],
                               ':primerApellido'=> $_POST['apellido1'],
                               ':segundoApellido'=> $_POST['apellido2'],
                               ':cedula' => $_POST['cedula'],
                               ':idDistrito'=> $_POST['distrito'],
                               ':direccion'=> $_POST['direccion'],
                               ':usuario'=>$_COOKIE[$user],
                               ':contrasena'=>$_POST['password']
                              ));

        echo '<script>alert("Usuario modificado exitosamente!");location="index.php";</script>';
    } else {
        echo'<script>alert("Contraseñas no coinciden");location="personas-edicion.php";</script>';
    }
} else {
        echo'<script>alert("Faltan Datos");location="personas-edicion.php";</script>';
}
?>
