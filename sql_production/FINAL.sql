-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for artesanos
CREATE DATABASE IF NOT EXISTS `artesanos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artesanos`;


-- Dumping structure for procedure artesanos.agregarCanton
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarCanton`(IN `pprovincia` INT, IN `pcanton` VARCHAR(50))
    COMMENT 'catalogo canton'
BEGIN
	INSERT INTO CANTON(id,idProvincia,descripcion)VALUES(NULL,pprovincia,pcanton);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarCliente
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarCliente`()
    COMMENT 'este procedimiento agrega un cliente'
BEGIN
	INSERT INTO CLIENTE(id,idPersona,ultima_visita)VALUES(NULL,(SELECT MAX(id) FROM persona),DEFAULT);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarColor
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarColor`(IN `pcolor` VARCHAR(50), IN `PorcentajeInf` FLOAT, IN `PorcentajeSup` FLOAT)
BEGIN
	INSERT INTO COLORSRM(id,descripcion,porcentaje_inferior,porcentaje_superior)VALUES(NULL,pcolor,PorcentajeInf,PorcentajeSup);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarDistrito
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarDistrito`(IN `pcanton` INT, IN `pdistrito` VARCHAR(50))
    COMMENT 'catalogo distrito'
BEGIN
	INSERT INTO DISTRITO(id,idCanton,descripcion)VALUES(NULL,pcanton,pdistrito);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarFamilia
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarFamilia`(IN `pfamilia` VARCHAR(50), IN `pidTipoFerm` INT)
BEGIN
	INSERT INTO familiacerveza(id,idTipoFermentacion,descripcion)VALUES(NULL,pidTipoFerm,pfamilia);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarProvincia
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarProvincia`(IN `pprovincia` VARCHAR(50))
    COMMENT 'agregar a catalogo'
BEGIN
	INSERT INTO PROVINCIA(id,idPais,descripcion)VALUES(NULL,1,pprovincia);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.agregarTipoCerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarTipoCerveza`(IN `ptipo` VARCHAR(50), IN `pfamilia` INT, IN `pcuerpo` VARCHAR(50))
BEGIN
	INSERT INTO tipocerveza(id,idFamilia,descripcion,cuerpo)VALUES(NULL,pfamilia,ptipo,pcuerpo);
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.bitacora
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `bitacora`(IN `pUsuario` VARCHAR(50), IN `pTabla` VARCHAR(50), IN `pColumna` VARCHAR(50), IN `pValorAnt` VARCHAR(50), IN `pValorAct` VARCHAR(50))
BEGIN
	INSERT INTO bitacora(id,idUsuario,fecha_mod,tabla_nombre,columna_nombre,valor_anterior,valor_actual)
	VALUES(NULL,pUsuario,default,pTabla,pColumna,pValorAnt,pValorAct);
END//
DELIMITER ;


-- Dumping structure for table artesanos.bitacora
CREATE TABLE IF NOT EXISTS `bitacora` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada registro en bitácora',
  `idUsuario` varchar(50) NOT NULL COMMENT 'Llave foránea referencia al Usuario que modifica un dato',
  `fecha_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se realia la modificación',
  `tabla_nombre` varchar(50) NOT NULL COMMENT 'Nombre de la tabla en la que se hace la edición',
  `columna_nombre` varchar(50) NOT NULL COMMENT 'Nombre de la columna donde se hace la edición',
  `valor_anterior` varchar(100) NOT NULL COMMENT 'Valor anterior del atributo editado',
  `valor_actual` varchar(100) NOT NULL COMMENT 'Valor con el que se edita el atributo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Registro e cambios en la base';

-- Dumping data for table artesanos.bitacora: ~2 rows (approximately)
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` (`id`, `idUsuario`, `fecha_mod`, `tabla_nombre`, `columna_nombre`, `valor_anterior`, `valor_actual`) VALUES
	(1, 'admin', '2016-06-07 19:59:52', 'Cerveza', 'Marca', 'THEWHO', 'potato'),
	(2, 'admin2', '2016-06-07 21:32:56', 'Cliente', 'Contraseña', 'asdf', 'admin2');
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;


-- Dumping structure for table artesanos.canton
CREATE TABLE IF NOT EXISTS `canton` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada cantón',
  `idProvincia` int(11) NOT NULL COMMENT 'Llave foránea identificador único de la provincia de cada cantón',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del Cantón',
  PRIMARY KEY (`id`),
  KEY `FK__provincia` (`idProvincia`),
  CONSTRAINT `FK__provincia` FOREIGN KEY (`idProvincia`) REFERENCES `provincia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COMMENT='Todos los cantones de todas las provincias de todos los países';

-- Dumping data for table artesanos.canton: ~91 rows (approximately)
/*!40000 ALTER TABLE `canton` DISABLE KEYS */;
INSERT INTO `canton` (`id`, `idProvincia`, `descripcion`) VALUES
	(1, 1, 'San José (Central)'),
	(2, 1, 'Escazú'),
	(3, 1, 'Desamparados'),
	(4, 1, 'Puriscal'),
	(5, 1, 'Tarrazú'),
	(6, 1, 'Aserrí'),
	(7, 1, 'Mora'),
	(8, 1, 'Goicoechea'),
	(9, 1, 'Santa Ana'),
	(10, 1, 'Alajuelita'),
	(11, 1, 'Vázquez de Coronado'),
	(12, 1, 'Acosta'),
	(13, 1, 'Tibás'),
	(14, 1, 'Moravia'),
	(15, 1, 'Montes de Oca'),
	(16, 1, 'Turrubares'),
	(17, 1, 'Dota'),
	(18, 1, 'Curridabat'),
	(19, 1, 'Pérez Zeledón'),
	(20, 1, 'León Cortés'),
	(21, 2, 'Alajuela (Central)'),
	(22, 2, 'San Ramón'),
	(23, 2, 'Grecia'),
	(24, 2, 'San Mateo'),
	(25, 2, 'Atenas'),
	(26, 2, 'Naranjo'),
	(27, 2, 'Palmares'),
	(28, 2, 'Poás'),
	(29, 2, 'Orotina'),
	(30, 2, 'San Carlos'),
	(31, 2, 'Alfaro Ruiz'),
	(32, 2, 'Valverde Vega'),
	(33, 2, 'Upala'),
	(34, 2, 'Los Chiles'),
	(35, 2, 'Guatuso'),
	(36, 3, 'Cartago (Central)'),
	(37, 3, 'Paraíso'),
	(38, 3, 'La Unión'),
	(39, 3, 'Jiménez'),
	(40, 3, 'Turrialba'),
	(41, 3, 'Alvarado'),
	(42, 3, 'Oreamuno'),
	(43, 3, 'El Guarco'),
	(44, 4, 'Heredia (Central)'),
	(45, 4, 'Barva'),
	(46, 4, 'Santo Domingo'),
	(47, 4, 'Santa Bárbara'),
	(48, 4, 'San Rafael'),
	(49, 4, 'San Isidro'),
	(50, 4, 'Belén'),
	(51, 4, 'Flores'),
	(52, 4, 'San Pablo'),
	(53, 4, 'Sarapiquí'),
	(54, 5, 'Liberia'),
	(55, 5, 'Nicoya'),
	(56, 5, 'Santa Cruz'),
	(57, 5, 'Bagaces'),
	(58, 5, 'Carrillo'),
	(59, 5, 'Cañas'),
	(60, 5, 'Abangares'),
	(61, 5, 'Tilarán'),
	(62, 5, 'Nandayure'),
	(63, 5, 'La Cruz'),
	(64, 5, 'Hojancha'),
	(65, 6, 'Puntarenas (Central)'),
	(66, 6, 'Esparza'),
	(67, 6, 'Buenos Aires'),
	(68, 6, 'Montes de Oro'),
	(69, 6, 'Osa'),
	(70, 6, 'Aguirre'),
	(71, 6, 'Golfito'),
	(72, 6, 'Coto Brus'),
	(73, 6, 'Parrita'),
	(74, 6, 'Corredores'),
	(75, 6, 'Garabito'),
	(76, 7, 'Limón (Central)'),
	(77, 7, 'Pococí'),
	(78, 7, 'Siquirres'),
	(79, 7, 'Talamanca'),
	(80, 7, 'Matina'),
	(81, 7, 'Guácimo'),
	(82, 8, 'Kingston'),
	(83, 9, 'Lambeth'),
	(84, 9, 'Westminister'),
	(85, 10, 'Trafford'),
	(86, 10, 'Stockport'),
	(87, 11, 'Buenos Aires'),
	(88, 12, 'Brooklyn'),
	(89, 12, 'Manhattan'),
	(90, 12, 'The Bronx'),
	(91, 13, 'Los Angeles');
/*!40000 ALTER TABLE `canton` ENABLE KEYS */;


-- Dumping structure for table artesanos.cerveza
CREATE TABLE IF NOT EXISTS `cerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de ',
  `idTipo` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al tipo.',
  `idColor` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al color de acuerdo al SRM.',
  `marca` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre comercial',
  `temperaturaFerm` int(11) NOT NULL DEFAULT '0' COMMENT 'Temperatura en grados Celsius',
  `tiempoFerm` int(11) NOT NULL DEFAULT '0' COMMENT 'Días en reposo.',
  `precioColones` int(11) NOT NULL DEFAULT '0' COMMENT 'Precio de venta en colones, 13% IVI incluido',
  `IBUamargura` int(11) NOT NULL DEFAULT '0' COMMENT 'Índice de amargura: International Bitterness Units',
  `ABValcohol` int(11) NOT NULL DEFAULT '0' COMMENT 'Índice de alcohol por volúmen',
  `image` longblob,
  PRIMARY KEY (`id`),
  KEY `FK_cerveza_tipo` (`idTipo`),
  KEY `FK_cerveza_color` (`idColor`),
  CONSTRAINT `FK_cerveza_color` FOREIGN KEY (`idColor`) REFERENCES `colorsrm` (`id`),
  CONSTRAINT `FK_cerveza_tipo` FOREIGN KEY (`idTipo`) REFERENCES `tipocerveza` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='Conjunto de bebidas alcohólicas a la venta.';

-- Dumping data for table artesanos.cerveza: ~23 rows (approximately)
/*!40000 ALTER TABLE `cerveza` DISABLE KEYS */;
INSERT INTO `cerveza` (`id`, `idTipo`, `idColor`, `marca`, `temperaturaFerm`, `tiempoFerm`, `precioColones`, `IBUamargura`, `ABValcohol`, `image`) VALUES
	(70, 9, 3, 'Saison Imperiale', 5, 20, 4500, 0, 9, NULL),
	(71, 9, 3, 'La Moneuse', 5, 20, 3000, 0, 8, NULL),
	(72, 29, 6, 'Equinox Dark Belgian Winter', 5, 20, 4000, 0, 8, NULL),
	(73, 16, 4, 'Serafijn Christmas Angel', 5, 20, 5800, 0, 8, NULL),
	(74, 50, 12, 'Obovoid Empirical Stout', 10, 10, 4600, 0, 8, NULL),
	(75, 17, 4, 'Furious Beer', 5, 20, 4700, 0, 6, NULL),
	(76, 33, 8, 'Bender Beer', 5, 20, 4200, 0, 5, NULL),
	(77, 42, 6, 'Oak Aged Ebenezer', 10, 10, 5000, 0, 9, NULL),
	(78, 64, 7, 'Killer Penguin', 7, 15, 5800, 0, 10, NULL),
	(79, 36, 12, 'Bourbon County Stout', 7, 15, 4700, 0, 13, NULL),
	(80, 60, 5, 'Twelve Days', 5, 20, 1800, 0, 6, NULL),
	(81, 60, 5, 'Seriously Bad Elf', 5, 20, 2500, 0, 9, NULL),
	(82, 64, 7, 'Harvest Ale', 7, 15, 1500, 0, 7, NULL),
	(83, 24, 2, 'Imperial', 10, 10, 800, 17, 5, NULL),
	(84, 16, 4, 'Duvel', 5, 20, 1000, 6, 9, NULL),
	(85, 25, 2, 'Heineken', 10, 10, 900, 23, 5, NULL),
	(86, 11, 1, 'Corona Light', 10, 10, 700, 7, 4, NULL),
	(87, 24, 2, 'Corona Extra', 10, 10, 700, 19, 5, NULL),
	(88, 24, 2, 'Imperial Silver', 10, 10, 800, 17, 5, NULL),
	(89, 51, 2, 'Pilsen', 7, 15, 800, 0, 5, NULL),
	(90, 39, 9, 'Bavaria Dark', 10, 10, 900, 0, 5, NULL),
	(91, 23, 1, 'Bavaria Gold', 7, 15, 950, 0, 5, NULL),
	(92, 38, 2, 'Rock Ice', 10, 10, 800, 0, 7, NULL);
/*!40000 ALTER TABLE `cerveza` ENABLE KEYS */;


-- Dumping structure for table artesanos.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada persona registrado como cliente',
  `idPersona` int(11) NOT NULL COMMENT 'Llave foránea referencia a los datos de la persona',
  `ultima_visita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la última visita del cliente al local cervecero',
  PRIMARY KEY (`id`),
  KEY `FKPersona` (`idPersona`),
  CONSTRAINT `FKPersona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='Personas que pertenezcan a este conjunto no serán consideradas empleadas del pub.';

-- Dumping data for table artesanos.cliente: ~9 rows (approximately)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`id`, `idPersona`, `ultima_visita`) VALUES
	(18, 72, '2016-06-08 10:57:57'),
	(19, 73, '2016-06-08 10:57:57'),
	(20, 74, '2016-06-08 10:57:57'),
	(21, 75, '2016-06-08 10:57:57'),
	(22, 76, '2016-06-08 10:57:57'),
	(23, 77, '2016-06-08 10:57:57'),
	(24, 78, '2016-06-08 10:57:57'),
	(25, 79, '2016-06-08 10:57:57'),
	(26, 80, '2016-06-08 10:57:57');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Dumping structure for table artesanos.colorsrm
CREATE TABLE IF NOT EXISTS `colorsrm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identifiador único de cada tipo de color SRM',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del tipo de color SRM',
  `porcentaje_inferior` float NOT NULL COMMENT 'Número menor del rango que determina si pertenece a este tipo de color',
  `porcentaje_superior` float NOT NULL COMMENT 'Número mayor del rango que determina si pertenece a este tipo de color',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Tabla de clasificación del SRM para cada cerveza';

-- Dumping data for table artesanos.colorsrm: ~12 rows (approximately)
/*!40000 ALTER TABLE `colorsrm` DISABLE KEYS */;
INSERT INTO `colorsrm` (`id`, `descripcion`, `porcentaje_inferior`, `porcentaje_superior`) VALUES
	(1, 'Pajizo', 1, 3),
	(2, 'Amarillo', 3, 4.5),
	(3, 'Dorado', 4.5, 7.5),
	(4, 'Ámbar', 7.5, 9),
	(5, 'Ámbar Profundo', 9, 11),
	(6, 'Cobre', 11, 14),
	(7, 'Marrón ligero', 14, 19),
	(8, 'Marrón', 19, 22),
	(9, 'Marrón Oscuro', 22, 30),
	(10, 'Marrón muy oscuro', 30, 35),
	(11, 'Negro', 35, 40),
	(12, 'Negro Opaco', 40, 50);
/*!40000 ALTER TABLE `colorsrm` ENABLE KEYS */;


-- Dumping structure for table artesanos.compras_por_cliente
CREATE TABLE IF NOT EXISTS `compras_por_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersona` int(11) NOT NULL COMMENT 'Llave primaria identificador único referencia de cada personas registrada como cliente',
  `idCerveza` int(11) NOT NULL COMMENT 'Llave primaria identificador único referencia de la cerveza comprada',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se realiza la compra',
  `cantidad` int(11) NOT NULL COMMENT 'Cantidad de cervezas compradas',
  PRIMARY KEY (`id`),
  KEY `idCerveza` (`idCerveza`),
  KEY `idPersona` (`idPersona`),
  CONSTRAINT `FK1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`),
  CONSTRAINT `FK2` FOREIGN KEY (`idCerveza`) REFERENCES `cerveza` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='Compras hechas por cada cliente.';

-- Dumping data for table artesanos.compras_por_cliente: ~12 rows (approximately)
/*!40000 ALTER TABLE `compras_por_cliente` DISABLE KEYS */;
INSERT INTO `compras_por_cliente` (`id`, `idPersona`, `idCerveza`, `fecha`, `cantidad`) VALUES
	(21, 72, 80, '2016-06-08 16:10:15', 3),
	(22, 72, 80, '2016-06-08 16:10:15', 2),
	(23, 73, 72, '2016-06-08 16:10:15', 5),
	(24, 80, 72, '2016-06-08 16:10:15', 1),
	(25, 80, 90, '2016-06-08 16:10:15', 1),
	(26, 81, 91, '2016-06-08 16:10:15', 2),
	(27, 77, 73, '2016-06-08 16:10:15', 3),
	(28, 78, 84, '2016-06-08 16:10:15', 10),
	(29, 79, 84, '2016-06-08 16:10:15', 9),
	(30, 75, 84, '2016-06-08 16:10:15', 7),
	(31, 77, 83, '2016-06-08 16:10:15', 2),
	(32, 80, 88, '2016-06-08 16:10:15', 4);
/*!40000 ALTER TABLE `compras_por_cliente` ENABLE KEYS */;


-- Dumping structure for procedure artesanos.crearCerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `crearCerveza`(IN `pidTipo` INT, IN `pidColor` INT, IN `pmarca` VARCHAR(50), IN `ptemperatura` INT, IN `ptiempo` INT, IN `pprecio` INT, IN `pIBU` INT, IN `pABV` INT, IN `pimagen` LONGBLOB)
    COMMENT 'este procedimiento crea na cerveza'
BEGIN
	INSERT INTO CERVEZA(id,idTipo,idColor,marca,temperaturaFerm,tiempoFerm,precioColones,IBUamargura,ABValcohol,image)
	VALUES(NULL,pidTipo,pidColor,pmarca,ptemperatura,ptiempo,pprecio,pIBU,pABV,pimagen);
	commit;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.crearPersona
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `crearPersona`(IN `pnombre` VARCHAR(50), IN `pprimerApellido` VARCHAR(50), IN `psegundoApellido` VARCHAR(50), IN `pnumeroCedula` INT, IN `pidDistrito` INT, IN `pdireccionExacta` VARCHAR(50), IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
    COMMENT 'Este procedimiento registra un usuario al sistema'
BEGIN
	INSERT INTO PERSONA(id,nombre,primerApellido,segundoApellido,numeroCedula,idDistrito,direccionExacta,usuario,contrasena)
	VALUES(NULL,pnombre,pprimerApellido,psegundoApellido,pnumeroCedula,pidDistrito,pdireccionExacta,pusuario,pcontrasena);
	commit;
END//
DELIMITER ;


-- Dumping structure for table artesanos.distrito
CREATE TABLE IF NOT EXISTS `distrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada distrito',
  `idCanton` int(11) NOT NULL COMMENT 'Llave foránea identificador único del canton al que pertenece el distrito',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del distrito',
  PRIMARY KEY (`id`),
  KEY `FK__canton` (`idCanton`),
  CONSTRAINT `FK__canton` FOREIGN KEY (`idCanton`) REFERENCES `canton` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8 COMMENT='Listado de todos los lugares registrados como distritos';

-- Dumping data for table artesanos.distrito: ~203 rows (approximately)
/*!40000 ALTER TABLE `distrito` DISABLE KEYS */;
INSERT INTO `distrito` (`id`, `idCanton`, `descripcion`) VALUES
	(1, 1, 'Carmen'),
	(2, 1, 'Merced'),
	(3, 1, 'Hospital'),
	(4, 1, 'Catedral'),
	(5, 1, 'Zapote'),
	(6, 1, 'San Francisco de Dos Ríos'),
	(7, 1, 'Uruca'),
	(8, 1, 'Mata Redonda'),
	(9, 1, 'Pavas'),
	(10, 1, 'Hatillo'),
	(11, 1, 'San Sebastián'),
	(12, 2, 'Escazú'),
	(13, 2, 'San Antonio'),
	(14, 2, 'San Rafael'),
	(15, 3, 'Desamparados'),
	(16, 3, 'San Miguel'),
	(17, 3, 'San Juan de Dios'),
	(18, 3, 'San Rafael Arriba'),
	(19, 3, 'San Antonio'),
	(20, 3, 'Frailes'),
	(21, 3, 'Patarrá'),
	(22, 3, 'San Cristobal'),
	(23, 3, 'Rosario'),
	(24, 3, 'Damas'),
	(25, 3, 'San Rafael Abajo'),
	(26, 3, 'Gravilias'),
	(27, 3, 'Los Guido'),
	(28, 4, 'Santiago'),
	(29, 4, 'Mercedes Sur'),
	(30, 4, 'Barbacoas'),
	(31, 4, 'Grifo Alto'),
	(32, 4, 'San Rafael'),
	(33, 4, 'Candelaria'),
	(34, 4, 'Desamparaditos'),
	(35, 4, 'San Antonio'),
	(36, 4, 'Chires'),
	(37, 5, 'San Marcos'),
	(38, 5, 'San Lorenzo'),
	(39, 5, 'San Carlos'),
	(40, 6, 'Tarbaca'),
	(41, 6, 'Vuelta de Jorco'),
	(42, 6, 'San Gabriel'),
	(43, 6, 'Tarbaca'),
	(44, 6, 'Legua'),
	(45, 6, 'Monterrey'),
	(46, 6, 'Salitrillos'),
	(47, 7, 'Colón'),
	(48, 7, 'Guayabo'),
	(49, 7, 'Tabarcia'),
	(50, 7, 'Piedras Negras'),
	(51, 7, 'Picagres'),
	(52, 8, 'Guadalupe'),
	(53, 8, 'San Francisco'),
	(54, 8, 'Calle Blancos'),
	(55, 8, 'Mata de Plátano'),
	(56, 8, 'Ipís'),
	(57, 8, 'Rancho Redondo'),
	(58, 8, 'Purral'),
	(59, 9, 'Santa Ana'),
	(60, 9, 'Salitral'),
	(61, 9, 'Pozos'),
	(62, 9, 'Uruca'),
	(63, 9, 'Piedades'),
	(64, 9, 'Brasil'),
	(65, 10, 'Alajuelita'),
	(66, 10, 'San Josecito'),
	(67, 10, 'San Antonio'),
	(68, 10, 'Concepción'),
	(69, 10, 'San Felipe'),
	(70, 11, 'San Isidro'),
	(71, 11, 'San Rafael'),
	(72, 11, 'Dulce Nombre de Jesús'),
	(73, 11, 'Patalillo'),
	(74, 11, 'Cascajal'),
	(75, 12, 'San Ignacio de Acosta'),
	(76, 12, 'Guaitil'),
	(77, 12, 'Palmichal'),
	(78, 12, 'Cangrejal'),
	(79, 12, 'Sabanillas'),
	(80, 13, 'San Juan'),
	(81, 13, 'Cisco Esquinas'),
	(82, 13, 'Anselmo Llorente'),
	(83, 13, 'León XIII'),
	(84, 13, 'Colima'),
	(85, 14, 'San Vicente'),
	(86, 14, 'San Jerónimo'),
	(87, 14, 'Trinidad'),
	(88, 15, 'San Pedro'),
	(89, 15, 'Sabanilla'),
	(90, 15, 'Mercedes'),
	(91, 15, 'San Rafael'),
	(92, 16, 'San Pablo'),
	(93, 16, 'San Pedro'),
	(94, 16, 'San Luis'),
	(95, 16, 'Carara'),
	(96, 17, 'Santa María'),
	(97, 17, 'Jardín'),
	(98, 17, 'Copey'),
	(99, 18, 'Curridabat'),
	(100, 18, 'Granadilla'),
	(101, 18, 'Sánchez'),
	(102, 18, 'Tirrases'),
	(103, 19, 'San Isidro del General'),
	(104, 19, 'General'),
	(105, 19, 'Daniel Flores'),
	(106, 19, 'Rivas'),
	(107, 19, 'San Pedro'),
	(108, 19, 'Platanares'),
	(109, 19, 'Pejibaye'),
	(110, 19, 'Cajón'),
	(111, 19, 'Barú'),
	(112, 19, 'Río Nuevo'),
	(113, 19, 'Páramo'),
	(114, 20, 'San Pablo'),
	(115, 20, 'San Andrés'),
	(116, 20, 'Llano Bonito'),
	(117, 20, 'San Isidro'),
	(118, 20, 'Santa Cruz'),
	(119, 20, 'San Antonio'),
	(120, 21, 'Alajuela (Centro)'),
	(121, 21, 'Río Segundo'),
	(122, 22, 'Piedades Norte'),
	(123, 23, 'Grecia'),
	(124, 24, 'Desmonte'),
	(125, 25, 'Concepción'),
	(126, 26, 'Palmitos'),
	(127, 27, 'Zaragoza'),
	(128, 28, 'Sabana Redonda'),
	(129, 29, 'Mastate'),
	(130, 30, 'Ciudad Quesada'),
	(131, 31, 'Zarcero'),
	(132, 32, 'Toro Amarillo'),
	(133, 33, 'Aguas Claras'),
	(134, 34, 'Caño Negro'),
	(135, 35, 'Buenavista'),
	(136, 36, 'Tierra Blanca'),
	(137, 36, 'Cartago'),
	(138, 37, 'Santiago'),
	(139, 38, 'Tres Ríos'),
	(140, 39, 'Pejibaye'),
	(141, 40, 'Turrialba'),
	(142, 41, 'Pacayas'),
	(143, 42, 'Cot'),
	(144, 43, 'Tejar'),
	(145, 44, 'Heredia'),
	(146, 44, 'Mercedes'),
	(147, 45, 'San Pedro'),
	(148, 46, 'Santo Tomás'),
	(149, 47, 'San Juan'),
	(150, 48, 'Concepción'),
	(151, 49, 'San Isidro'),
	(152, 50, 'San Antonio'),
	(153, 51, 'San Joaquín de Flores'),
	(154, 52, 'San Pablo'),
	(155, 53, 'Horquetas'),
	(156, 54, 'Liberia'),
	(157, 55, 'Mansion'),
	(158, 56, 'Cartagena'),
	(159, 57, 'Bagaces'),
	(160, 58, 'Sardinal'),
	(161, 59, 'Cañas'),
	(162, 60, 'Colorado'),
	(163, 61, 'Santa Rosa'),
	(164, 62, 'Santa Rita'),
	(165, 63, 'La Cruz'),
	(166, 64, 'Puerto Carrillo'),
	(167, 65, 'Puntarenas'),
	(168, 65, 'Monte Verde'),
	(169, 66, 'Macacona'),
	(170, 67, 'Potrero Grande'),
	(171, 68, 'Miramar'),
	(172, 69, 'Puerto Cortés'),
	(173, 70, 'Quepos'),
	(174, 71, 'Puerto Jiménez'),
	(175, 71, 'Golfito'),
	(176, 72, 'San Vito'),
	(177, 73, 'Parrita'),
	(178, 74, 'Corredor'),
	(179, 75, 'Jacó'),
	(180, 76, 'Limón'),
	(181, 76, 'Valle de la Estrella'),
	(182, 77, 'Roxana'),
	(183, 78, 'Siquirres'),
	(184, 79, 'Sixaola'),
	(185, 80, 'Batán'),
	(186, 81, 'Pocora'),
	(187, 82, 'Harbor View'),
	(188, 83, 'Kennington'),
	(189, 83, 'Brixton'),
	(190, 84, 'Bayswater'),
	(191, 84, 'Paddington'),
	(192, 85, 'Flixton'),
	(193, 86, 'Heaton Chapel'),
	(194, 87, 'Caballito'),
	(195, 87, 'Monte Castro'),
	(196, 87, 'San Cristóbal'),
	(197, 88, 'Brooklyn Heights'),
	(198, 88, 'Mapleton'),
	(199, 89, 'Marble Hill'),
	(200, 90, 'Castle Hill'),
	(201, 90, 'Williamsbridge'),
	(202, 91, 'Boyle Heights'),
	(203, 91, 'Chinatown');
/*!40000 ALTER TABLE `distrito` ENABLE KEYS */;


-- Dumping structure for table artesanos.familiacerveza
CREATE TABLE IF NOT EXISTS `familiacerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de la familia de cerveza',
  `idTipoFermentacion` int(11) NOT NULL COMMENT 'Llave foránea identificador del tipo de fermentación que tiene la familia ',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre de la familia de cervezas',
  PRIMARY KEY (`id`),
  KEY `FK__tipofermentacion` (`idTipoFermentacion`),
  CONSTRAINT `FK__tipofermentacion` FOREIGN KEY (`idTipoFermentacion`) REFERENCES `tipofermentacion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Grandes familias de cerveza a nivel mundial.';

-- Dumping data for table artesanos.familiacerveza: ~20 rows (approximately)
/*!40000 ALTER TABLE `familiacerveza` DISABLE KEYS */;
INSERT INTO `familiacerveza` (`id`, `idTipoFermentacion`, `descripcion`) VALUES
	(1, 4, 'Trigo'),
	(2, 3, 'Lambic y Ales Amargas'),
	(3, 1, 'Belgian Ale'),
	(4, 1, 'Pale Ale'),
	(5, 4, 'English Bitter'),
	(6, 1, 'Scottish Ale'),
	(7, 1, 'Brown Ale'),
	(8, 4, 'Porter'),
	(9, 4, 'Stout'),
	(10, 4, 'Pilsner'),
	(11, 2, 'American Lager'),
	(12, 2, 'European Lager'),
	(13, 4, 'Bock'),
	(14, 4, 'Alt'),
	(15, 1, 'French Ale'),
	(16, 1, 'German Amber Ale'),
	(17, 4, 'American Special'),
	(18, 4, 'Smoked Beer'),
	(19, 4, 'Barley Wine'),
	(20, 1, 'Strong Ale');
/*!40000 ALTER TABLE `familiacerveza` ENABLE KEYS */;


-- Dumping structure for procedure artesanos.getBitacoras
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getBitacoras`()
BEGIN
	SELECT * FROM BITACORA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getCanton
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCanton`()
    COMMENT 'Este procedimiento selecciona los cantones'
BEGIN
	SELECT id,idProvincia,descripcion FROM CANTON;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getCedulalike
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCedulalike`(IN `pcedula` INT)
    COMMENT 'Este procedimiento selecciona el numero de cedula'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.numeroCedula LIKE CONCAT('%', pcedula,'%');
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getCerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCerveza`()
BEGIN
	SELECT * FROM CERVEZA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getCervezaById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCervezaById`(IN `pid` INT)
BEGIN
	SELECT * FROM CERVEZA
	WHERE id = pid;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getCervezaByMarca
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCervezaByMarca`(IN `pmarca` VARCHAR(50))
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM cerveza
	WHERE cerveza.marca LIKE CONCAT('%', pmarca,'%');
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getClienteById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClienteById`(IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
    COMMENT 'Este procedimiento se utiliza para el registro'
BEGIN
	SELECT *
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.usuario=pusuario and persona.contrasena=pcontrasena;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getClienteByUser
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClienteByUser`(IN `pusuario` VARCHAR(50))
BEGIN
	SELECT *
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.usuario=pusuario;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getClientes
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClientes`()
    COMMENT 'Este procedimiento selecciona los clientes'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on  persona.id = cliente.idPersona;

END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getColorById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getColorById`(IN `pidColor` INT)
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM colorsrm
	LEFT JOIN cerveza on cerveza.idColor = colorsrm.id
	WHERE colorsrm.id=pidColor;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getColores
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getColores`()
    COMMENT 'Este procedimiento retorna los colores'
BEGIN
	SELECT * FROM COLORSRM;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getComprasCliente
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasCliente`(IN `pID` INT)
BEGIN
	select  c.marca, cxc.cantidad, cxc.fecha from cerveza as c, compras_por_cliente as cxc, persona as p
	where cxc.idCerveza = c.id and cxc.idPersona = p.id and p.id=pID;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getComprasPorNombre
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasPorNombre`(IN `pUsuario` VARCHAR(50))
BEGIN
	select  c.marca, cxc.cantidad, cxc.fecha from cerveza as c, compras_por_cliente as cxc, persona as p
	where cxc.idCerveza = c.id and cxc.idPersona = p.id and p.usuario=pUsuario;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getComprasPorNombreBack
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasPorNombreBack`(IN `pUsuario` VARCHAR(50))
BEGIN
	SELECT c.marca, SUM(cantidad) as suma
	FROM persona AS p, cerveza AS c, compras_por_cliente AS cxc
	WHERE cxc.idCerveza = c.id AND cxc.idPersona = p.id AND p.usuario = pUsuario
	GROUP BY idCerveza;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getDistrito
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getDistrito`()
    COMMENT 'Este procedimiento selecciona los distritos'
BEGIN
	SELECT id,idCanton,descripcion FROM DISTRITO;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getFamilias
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getFamilias`()
    COMMENT 'Este procedimiento retorna las familias'
BEGIN
	SELECT * FROM FAMILIACERVEZA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getImagenes
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getImagenes`()
BEGIN
	SELECT * FROM IMAGENES;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getNombrelike
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getNombrelike`(IN `nombre` VARCHAR(50))
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.nombre LIKE CONCAT('%', nombre,'%');
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getPersona
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPersona`(IN `nombreUsuario` VARCHAR(50))
    COMMENT 'este procedimiento selecciona todos los datos de la persona'
BEGIN
	SELECT * FROM PERSONA
	WHERE usuario = nombreUsuario;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getPersonabyId
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPersonabyId`()
    COMMENT 'Este procedimiento selecciona la persona por id'
BEGIN
	SELECT * FROM PERSONA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getPrimerApellidolike
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPrimerApellidolike`(IN `pprimerApellido` VARCHAR(50))
    COMMENT 'este procedimiento selecciona por apellido1'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.primerApellido LIKE CONCAT('%', pprimerApellido,'%');
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getProvincia
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getProvincia`()
    COMMENT 'Este procedimiento selecciona las provincias'
BEGIN
	SELECT id,descripcion FROM PROVINCIA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getSegundoApellidolike
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getSegundoApellidolike`(IN `psegundoApellido` VARCHAR(50))
    COMMENT 'este procedimiento selecciona el apellido2'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.segundoApellido LIKE CONCAT('%', psegundoApellido,'%');
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getTipoCerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoCerveza`()
    COMMENT 'Este proc retorna los tipos de cerveza'
BEGIN
	SELECT * FROM TIPOCERVEZA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getTipoCervezaById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoCervezaById`(IN `pidTipo` INT)
    COMMENT 'Este procedimiento selecciona dependiendo del ID'
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM tipocerveza
	LEFT JOIN cerveza on cerveza.idTipo = tipocerveza.id
	WHERE tipocerveza.id=pidTipo;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getTipoFerm
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoFerm`()
BEGIN
	SELECT * FROM tipofermentacion;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getUsuario
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getUsuario`(IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
    COMMENT 'Este procedimiento selecciona los usuarios'
BEGIN
	SELECT id FROM PERSONA
	where usuario=pusuario and contrasena=pcontrasena;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.modificarCerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `modificarCerveza`(IN `pid` INT, IN `pidTipo` INT, IN `pidColor` INT, IN `pmarca` VARCHAR(50), IN `ptemperatura` INT, IN `ptiempoFerm` INT, IN `pprecio` INT, IN `pIBU` INT, IN `pABV` INT, IN `pimage` LONGBLOB)
BEGIN
	UPDATE CERVEZA
	SET idTipo=pidTipo,idColor=pidColor,marca=pmarca,temperaturaFerm=ptemperatura,tiempoFerm=ptiempoFerm,precioColones=pprecio,IBUamargura=pIBU,ABValcohol=pABV,image=pimage
	WHERE id=pid;
	commit;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.modificarUsuario
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `modificarUsuario`(IN `pnombre` VARCHAR(50), IN `papellido1` VARCHAR(50), IN `papellido2` VARCHAR(50), IN `pcedula` INT, IN `pidDistrito` INT, IN `pdireccion` VARCHAR(50), IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
    COMMENT 'Este procedimiento modifica a un usuario'
BEGIN
	UPDATE PERSONA
	SET nombre=pnombre,primerApellido=papellido1,segundoApellido=papellido2,numeroCedula=pcedula,idDistrito=pidDistrito,direccionExacta=pdireccion,contrasena=pcontrasena
	WHERE usuario=pusuario;
	commit;
END//
DELIMITER ;


-- Dumping structure for table artesanos.pais
CREATE TABLE IF NOT EXISTS `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del país',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del País',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Todos los paises registrados en la base de datos';

-- Dumping data for table artesanos.pais: ~5 rows (approximately)
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` (`id`, `descripcion`) VALUES
	(1, 'Costa Rica'),
	(2, 'Jamaica'),
	(3, 'Argentina'),
	(4, 'Inglaterra'),
	(5, 'Estados Unidos');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;


-- Dumping structure for table artesanos.parametros
CREATE TABLE IF NOT EXISTS `parametros` (
  `id` int(11) NOT NULL COMMENT 'Llave primaria identificador único de cada dato parametrizable',
  `nombre_parametro` varchar(50) NOT NULL COMMENT 'Nombre descriptivo de cada dato parametrizable',
  `usuario_ult_mod` int(11) NOT NULL COMMENT 'Usuario que modific[o por [ultima vez el parámetro',
  `fecha_ult_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se modificó',
  PRIMARY KEY (`id`),
  KEY `FK_usuario_last_mod` (`usuario_ult_mod`),
  CONSTRAINT `FK_usuario_last_mod` FOREIGN KEY (`usuario_ult_mod`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Parámetros del sistema.';

-- Dumping data for table artesanos.parametros: ~0 rows (approximately)
/*!40000 ALTER TABLE `parametros` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametros` ENABLE KEYS */;


-- Dumping structure for table artesanos.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de ',
  `nombre` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre de pila de la persona',
  `primerApellido` varchar(50) NOT NULL DEFAULT '0',
  `segundoApellido` varchar(50) NOT NULL DEFAULT '0',
  `numeroCedula` int(11) NOT NULL DEFAULT '0' COMMENT 'Documento de identificacion costarricense',
  `idDistrito` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al distrito de residencia.',
  `direccionExacta` varchar(300) NOT NULL DEFAULT '0',
  `usuario` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre de usuario.',
  `contrasena` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Encriptacion con SHA-2.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `FK_persona_distrito` (`idDistrito`),
  CONSTRAINT `FK_persona_distrito` FOREIGN KEY (`idDistrito`) REFERENCES `distrito` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8 COMMENT='Usuarios registrados. Nótese que cada persona se considera empleada por defecto hasta que se le registre como cliente.';

-- Dumping data for table artesanos.persona: ~11 rows (approximately)
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`id`, `nombre`, `primerApellido`, `segundoApellido`, `numeroCedula`, `idDistrito`, `direccionExacta`, `usuario`, `contrasena`) VALUES
	(30, 'Edwin', 'Cen', 'Xu', 702370779, 1, 'Avenida 6', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
	(72, 'Roy', 'Barnes', 'Perez', 123456790, 65, 'Frente a la Iglesia', 'rbar', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(73, 'Andrés', 'Peña', 'Castillo', 123456791, 133, 'Del Bar 500 N', 'apena', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(74, 'Seidi', 'Acevedo', 'Lopez', 123456792, 120, 'Detrás de la plaza', 'sacev', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(75, 'Mayra', 'Benavides', 'Pineda', 123456793, 1, 'Super La puebla', 'mbena', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(76, 'Eugenia', 'Calderon', 'Quesada', 123456794, 45, 'Casa J10', 'ecald', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(77, 'Francisco', 'Corrales', 'Viquez', 123456795, 20, 'Casa B5', 'fcorr', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(78, 'Gilbert', 'Espinoza', 'Thompson', 123456796, 21, 'Boutique Estilo', 'gespi', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(79, 'Jesús', 'Salazar', 'Gutierrez', 123456797, 35, 'De la iglesia 45m O', 'jsala', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(80, 'Yolanda', 'López', 'Camacho', 123456798, 40, 'Rancho Camacho ', 'ylope', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0'),
	(81, 'Lidieth', 'Monge', 'Moya', 123456799, 69, 'Del colegio 500m Sur', 'lmong', '99800b85d3383e3a2fb45eb7d0066a4879a9dad0');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;


-- Dumping structure for table artesanos.produccioncervezas
CREATE TABLE IF NOT EXISTS `produccioncervezas` (
  `idPais` int(11) NOT NULL COMMENT 'Llave primaria identificador único del país productor de cervezas',
  `idCerveza` int(11) NOT NULL COMMENT 'Llave primaria identificador único de la cerveza producida en el país',
  PRIMARY KEY (`idPais`,`idCerveza`),
  KEY `FKCERVEZA` (`idCerveza`),
  KEY `FKPAIS` (`idPais`),
  CONSTRAINT `FKCERVEZA` FOREIGN KEY (`idCerveza`) REFERENCES `cerveza` (`id`),
  CONSTRAINT `FKPAIS` FOREIGN KEY (`idPais`) REFERENCES `pais` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catálogo donde cada cerveza se produce.';

-- Dumping data for table artesanos.produccioncervezas: ~0 rows (approximately)
/*!40000 ALTER TABLE `produccioncervezas` DISABLE KEYS */;
/*!40000 ALTER TABLE `produccioncervezas` ENABLE KEYS */;


-- Dumping structure for table artesanos.provincia
CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de la provincia',
  `idPais` int(11) NOT NULL COMMENT 'Llave foránea identificador del país de la provincia',
  `descripcion` varchar(50) NOT NULL COMMENT 'Npmbre de la Provincia',
  PRIMARY KEY (`id`),
  KEY `FK__pais` (`idPais`),
  CONSTRAINT `FK__pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Todas las provincias de todos los paises de la base de datos';

-- Dumping data for table artesanos.provincia: ~13 rows (approximately)
/*!40000 ALTER TABLE `provincia` DISABLE KEYS */;
INSERT INTO `provincia` (`id`, `idPais`, `descripcion`) VALUES
	(1, 1, 'San José'),
	(2, 1, 'Alajuela'),
	(3, 1, 'Cartago'),
	(4, 1, 'Heredia'),
	(5, 1, 'Guanacaste'),
	(6, 1, 'Puntarenas'),
	(7, 1, 'Limón'),
	(8, 2, 'Kingston'),
	(9, 4, 'Londres'),
	(10, 4, 'Manchester'),
	(11, 3, 'Buenos Aires'),
	(12, 5, 'New York'),
	(13, 5, 'California');
/*!40000 ALTER TABLE `provincia` ENABLE KEYS */;


-- Dumping structure for procedure artesanos.registrarCompra
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `registrarCompra`(IN `pidPersona` INT, IN `pidCerveza` INT, IN `pcantidad` INT)
BEGIN
	INSERT INTO COMPRAS_POR_CLIENTE(id,idPersona,idCerveza,fecha,cantidad)VALUES(NULL,pidPersona,pidCerveza,default,pcantidad);
END//
DELIMITER ;


-- Dumping structure for table artesanos.tipocerveza
CREATE TABLE IF NOT EXISTS `tipocerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del tipo de cerveza ',
  `idFamilia` int(11) NOT NULL DEFAULT '0' COMMENT 'Llave foránea identificador de la familia del tipo de cerveza',
  `descripcion` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre del tipo de cerveza',
  `cuerpo` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nivel de espesura.',
  PRIMARY KEY (`id`),
  KEY `FK__familiacerveza` (`idFamilia`),
  CONSTRAINT `FK__familiacerveza` FOREIGN KEY (`idFamilia`) REFERENCES `familiacerveza` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='Estilos de cerveza por su sabor.';

-- Dumping data for table artesanos.tipocerveza: ~65 rows (approximately)
/*!40000 ALTER TABLE `tipocerveza` DISABLE KEYS */;
INSERT INTO `tipocerveza` (`id`, `idFamilia`, `descripcion`, `cuerpo`) VALUES
	(1, 1, 'Berliner Weisse', 'Ligera'),
	(2, 2, 'Lambic', 'Ligera'),
	(3, 3, 'Belgican Gold Ale', 'Ligera'),
	(4, 1, 'Belgican White', 'Ligera'),
	(5, 2, 'Gueuze', 'Ligera'),
	(6, 3, 'Tripel', 'Ligera'),
	(7, 1, 'American Wheat', 'Ligera'),
	(8, 2, 'Faro', 'Ligera'),
	(9, 3, 'Season', 'Ligera'),
	(10, 4, 'Pale Ale', 'Ligera'),
	(11, 11, 'American Lite', 'Pesada'),
	(12, 12, 'Munich Helles', 'Pesada'),
	(13, 13, 'Helles Bock', 'Pesada'),
	(14, 1, 'Weizenbier', 'Ligera'),
	(15, 2, 'Fruit Beer', 'Ligera'),
	(16, 3, 'Belgican Pale Ale', 'Ligera'),
	(17, 4, 'American Pale Ale', 'Ligera'),
	(18, 5, 'Ordinary Bitter', 'Ligera'),
	(19, 6, 'Scottish Light 60', 'Ligera'),
	(20, 7, 'English Mild', 'Ligera'),
	(21, 9, 'Dry Stout', 'Pesada'),
	(22, 9, 'Foreing Extra Stout', 'Pesada'),
	(23, 10, 'German Pilsner', 'Pesada'),
	(24, 11, 'American Standard', 'Pesada'),
	(25, 12, 'Dortmunder', 'Pesada'),
	(26, 13, 'Doppelbock', 'Pesada'),
	(27, 1, 'Dunkelweizen', 'Ligera'),
	(28, 2, 'Flanders Red', 'Ligera'),
	(29, 3, 'Belgican Dark Ale', 'Ligera'),
	(30, 4, 'Indian Pale Ale', 'Ligera'),
	(31, 5, 'Special Bitter', 'Ligera'),
	(32, 6, 'Scottish Heavy 70', 'Ligera'),
	(33, 7, 'American Brown', 'Ligera'),
	(34, 8, 'Brown Porter', 'Medio'),
	(35, 9, 'Sweet Stout', 'Pesada'),
	(36, 9, 'Imperial Stout', 'Pesada'),
	(37, 10, 'Bohemian Pilsner', 'Pesada'),
	(38, 11, 'American Premium', 'Pesada'),
	(39, 12, 'Munich Dunkel', 'Pesada'),
	(40, 13, 'Traditional Bock', 'Pesada'),
	(41, 1, 'Weizenbock', 'Ligera'),
	(42, 2, 'Oud Bruin', 'Ligera'),
	(43, 3, 'Dubbel', 'Ligera'),
	(44, 4, 'American Amber Ale', 'Ligera'),
	(45, 5, 'Extra Special Bitter', 'Ligera'),
	(46, 6, 'Scottish Export 80', 'Ligera'),
	(47, 7, 'English Brown', 'Ligera'),
	(48, 8, 'Robust Porter', 'Medio'),
	(49, 9, 'Oatmeal Stout', 'Pesada'),
	(50, 9, 'Russian Imperial Stout', 'Pesada'),
	(51, 10, 'American Pilsner', 'Pesada'),
	(52, 11, 'American Dark', 'Pesada'),
	(53, 12, 'Schwarzbier', 'Pesada'),
	(54, 13, 'Eisbock', 'Pesada'),
	(55, 14, 'Kolsch', 'Medio'),
	(56, 15, 'Biere de Garde', 'Medio'),
	(57, 16, 'Oktoberfest', 'Medio'),
	(58, 17, 'Cream Ale', 'Medio'),
	(59, 18, 'Smoked Beer', 'Medio'),
	(60, 20, 'English Old Ale', 'Medio'),
	(61, 14, 'Altbier', 'Medio'),
	(62, 16, 'Vienna', 'Medio'),
	(63, 17, 'Steam Beer', 'Ligera'),
	(64, 19, 'Barleywine', 'Medio'),
	(65, 20, 'Strong Scoth Ale', 'Medio');
/*!40000 ALTER TABLE `tipocerveza` ENABLE KEYS */;


-- Dumping structure for table artesanos.tipofermentacion
CREATE TABLE IF NOT EXISTS `tipofermentacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del Tipo de fermentación',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del tipo de fermentación',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Estilos de fabricación según las cervecerías.';

-- Dumping data for table artesanos.tipofermentacion: ~4 rows (approximately)
/*!40000 ALTER TABLE `tipofermentacion` DISABLE KEYS */;
INSERT INTO `tipofermentacion` (`id`, `descripcion`) VALUES
	(1, 'Fermentación Alta'),
	(2, 'Fermentación Baja'),
	(3, 'Fermentación Espontánea'),
	(4, 'Fermentación Mixta');
/*!40000 ALTER TABLE `tipofermentacion` ENABLE KEYS */;


-- Dumping structure for procedure artesanos.topCervezas
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topCervezas`(IN `top` INT)
    COMMENT 'Ranking de Cervezas más vendidas'
BEGIN
	SELECT c.marca, SUM(cantidad) as cantidadVendidas
	FROM compras_por_cliente AS cxc, cerveza AS c
	WHERE cxc.idCerveza = c.id
	GROUP BY idCerveza
	ORDER BY cantidadVendidas DESC
	LIMIT top;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.topCervezasXCliente
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topCervezasXCliente`(IN `pUsuario` INT)
    COMMENT 'Cervezas mas compradas por un solo cliente'
BEGIN
	SELECT c.marca, SUM(cantidad) as suma
	FROM persona AS p, cerveza AS c, compras_por_cliente AS cxc
	WHERE cxc.idCerveza = c.id AND cxc.idPersona = pUsuario
	GROUP BY idCerveza;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.topClientes
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topClientes`(IN `top` INT)
    COMMENT 'Ranking de los clientes que más compran cervezas'
BEGIN
	SELECT p.usuario, p.nombre, p.primerApellido,p.segundoApellido, SUM(cantidad) as cantidadCervezas
	FROM persona AS p, compras_por_cliente AS cxc
	WHERE cxc.idPersona = p.id
	GROUP BY idPersona
	ORDER BY cantidadCervezas DESC
	LIMIT top; -- Top Parametrizable!
END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

CREATE TABLE `correos` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`msg` LONGTEXT NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`sent` TINYINT(4) NOT NULL,
	PRIMARY KEY (`id`)
)
COMMENT='Correos por enviar.'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

DELIMITER //
CREATE EVENT `crearCorreoCervezasNuevas`
	ON SCHEDULE
		EVERY 5 MINUTE STARTS '2016-06-08 23:03:00'
	ON COMPLETION PRESERVE
	ENABLE
	COMMENT 'Genera correos para ser enviados al administrador'
	DO BEGIN
	SELECT GROUP_CONCAT(msg SEPARATOR ';') FROM
	(SELECT CONCAT_WS(',', marca,tipo,color) as msg FROM
		(SELECT cv.marca, tc.descripcion as tipo, col.descripcion as color FROM cerveza as cv, tipocerveza as tc, colorsrm as col
			WHERE cv.idTipo = tc.id AND cv.idColor = col.id AND DATE(cv.fechaRegistro) = DATE(NOW())) as filtro
	) as filtro2 into @mensaje;
	INSERT INTO correos (id, msg, sent) VALUES (NULL, @mensaje, false);
END
DELIMITER ;
