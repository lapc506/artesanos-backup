-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.14-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

-- Volcando estructura de base de datos para artesanos
CREATE DATABASE IF NOT EXISTS `artesanos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artesanos`;

-- Volcando estructura para tabla artesanos.pais

CREATE TABLE IF NOT EXISTS `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del país',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del País',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Todos los paises registrados en la base de datos';


-- Volcando estructura para tabla artesanos.provincia

CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de la provincia',
  `idPais` int(11) NOT NULL COMMENT 'Llave foránea identificador del país de la provincia',
  `descripcion` varchar(50) NOT NULL COMMENT 'Npmbre de la Provincia',
  PRIMARY KEY (`id`),
  KEY `FK__pais` (`idPais`),
  CONSTRAINT `FK__pais` FOREIGN KEY (`idPais`) REFERENCES `pais` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Todas las provincias de todos los paises de la base de datos';


-- Volcando estructura para tabla artesanos.canton
CREATE TABLE IF NOT EXISTS `canton` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada cantón',
  `idProvincia` int(11) NOT NULL COMMENT 'Llave foránea identificador único de la provincia de cada cantón',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del Cantón',
  PRIMARY KEY (`id`),
  KEY `FK__provincia` (`idProvincia`),
  CONSTRAINT `FK__provincia` FOREIGN KEY (`idProvincia`) REFERENCES `provincia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Todos los cantones de todas las provincias de todos los países';


-- Volcando estructura para tabla artesanos.distrito
CREATE TABLE IF NOT EXISTS `distrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de cada distrito',
  `idCanton` int(11) NOT NULL COMMENT 'Llave foránea identificador único del canton al que pertenece el distrito',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del distrito',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK__canton` FOREIGN KEY (`idCanton`) REFERENCES `canton` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Listado de todos los lugares registrados como distritos';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.tipofermentacion
CREATE TABLE IF NOT EXISTS `tipofermentacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del Tipo de fermentación',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre del tipo de fermentación',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Estilos de fabricación según las cervecerías.';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla artesanos.familiacerveza
CREATE TABLE IF NOT EXISTS `familiacerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de la familia de cerveza',
  `idTipoFermentacion` int(11) NOT NULL COMMENT 'Llave foránea identificador del tipo de fermentación que tiene la familia ',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre de la familia de cervezas',
  PRIMARY KEY (`id`),
  KEY `FK__tipofermentacion` (`idTipoFermentacion`),
  CONSTRAINT `FK__tipofermentacion` FOREIGN KEY (`idTipoFermentacion`) REFERENCES `tipofermentacion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Grandes familias de cerveza a nivel mundial.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.tipocerveza
CREATE TABLE IF NOT EXISTS `tipocerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del tipo de cerveza ',
  `idFamilia` int(11) NOT NULL DEFAULT '0' COMMENT 'Llave foránea identificador de la familia del tipo de cerveza',
  `descripcion` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre del tipo de cerveza',
  `cuerpo` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Tipo de espesura.',
  PRIMARY KEY (`id`),
  KEY `FK__familiacerveza` (`idFamilia`),
  CONSTRAINT `FK__familiacerveza` FOREIGN KEY (`idFamilia`) REFERENCES `familiacerveza` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Estilos de cerveza por su sabor.';

-- La exportación de datos fue deseleccionada.

-- Color de cerveza SRM
CREATE TABLE `colorsrm` (
	`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identifiador único de cada tipo de color SRM',
	`descripcion` VARCHAR(50) NOT NULL COMMENT 'Nombre del tipo de color SRM',
	`porcentaje_inferior` FLOAT NOT NULL COMMENT 'Número menor del rango que determina si pertenece a este tipo de color',
	`porcentaje_superior` FLOAT NOT NULL COMMENT 'Número mayor del rango que determina si pertenece a este tipo de color',
	PRIMARY KEY (`id`)
)COLLATE='utf8_general_ci' ENGINE=InnoDB COMMENT='Tabla de clasificación del SRM para cada cerveza' 
;

-- Volcando estructura para tabla artesanos.estilocerveza
-- CREATE TABLE IF NOT EXISTS `estilocerveza` (
--   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único del estilo de cerveza ',
--   `descripcion` varchar(50) NOT NULL COMMENT 'Nombre descriptivo del Estilo',
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Estilos de cerveza.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.cerveza
CREATE TABLE IF NOT EXISTS `cerveza` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de ',
  `idTipo` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al tipo.',
-- `idEstilo` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al estilo.',
  `idColor` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al color de acuerdo al SRM.',
  `marca` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre comercial',
  `temperaturaFerm` int(11) NOT NULL DEFAULT '0' COMMENT 'Temperatura en grados Celsius',
  `tiempoFerm` int(11) NOT NULL DEFAULT '0' COMMENT 'Días en reposo.',
  `precioColones` int(11) NOT NULL DEFAULT '0' COMMENT 'Precio de venta en colones, 13% IVI incluido',
  `IBUamargura` int(11) NOT NULL DEFAULT '0' COMMENT 'Índice de amargura: International Bitterness Units',
  `ABValcohol` int(11) NOT NULL DEFAULT '0' COMMENT 'Índice de alcohol por volúmen',
  `fechaRegistro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de ingreso de la cerveza.',
  PRIMARY KEY (`id`),
  KEY `FK_cerveza_tipo` (`idTipo`),
-- KEY `FK_cerveza_estilo` (`idEstilo`),
  KEY `FK_cerveza_color` (`idColor`),
-- CONSTRAINT `FK_cerveza_estilo` FOREIGN KEY (`idEstilo`) REFERENCES `estilocerveza` (`id`),
  CONSTRAINT `FK_cerveza_tipo` FOREIGN KEY (`idTipo`) REFERENCES `tipocerveza` (`id`),
  CONSTRAINT `FK_cerveza_color` FOREIGN KEY (`idColor`) REFERENCES `colorsrm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Conjunto de bebidas alcohólicas a la venta.';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla artesanos.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Llave primaria identificador único de ',
  `nombre` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre de pila de la persona',
  `primerApellido` varchar(50) NOT NULL DEFAULT '0',
  `segundoApellido` varchar(50) NOT NULL DEFAULT '0',
  `numeroCedula` int(11) NOT NULL DEFAULT '0' COMMENT 'Documento de identificacion costarricense',
  `idDistrito` int(11) NOT NULL DEFAULT '0' COMMENT 'Referencia al distrito de residencia.',
  `direccionExacta` varchar(300) NOT NULL DEFAULT '0',
  `usuario` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Nombre de usuario.',
  `contrasena` varchar(50) NOT NULL DEFAULT '0' COMMENT 'Encriptacion con SHA-2.',
  PRIMARY KEY (`id`),
  KEY `FK_persona_distrito` (`idDistrito`),
  CONSTRAINT `FK_persona_distrito` FOREIGN KEY (`idDistrito`) REFERENCES `distrito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuarios registrados. Nótese que cada persona se considera empleada por defecto hasta que se le registre como cliente.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL COMMENT 'Llave primaria identificador único de cada persona registrado como cliente',
  `idPersona` int(11) NOT NULL COMMENT 'Llave foránea referencia a los datos de la persona',
  `ultima_visita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha de la última visita del cliente al local cervecero',
  PRIMARY KEY (`id`),
  KEY `FKPersona` (`idPersona`) ,
  CONSTRAINT `FKPersona` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Personas que pertenezcan a este conjunto no serán consideradas empleadas del pub.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.produccioncervezas
CREATE TABLE IF NOT EXISTS `produccioncervezas` (
  `idPais` int(11) NOT NULL COMMENT 'Llave primaria identificador único del país productor de cervezas',
  `idCerveza` int(11) NOT NULL COMMENT 'Llave primaria identificador único de la cerveza producida en el país',
  PRIMARY KEY (`idPais`,`idCerveza`),
  KEY `FKCERVEZA` (`idCerveza`),
  KEY `FKPAIS` (`idPais`),
  CONSTRAINT `FKCERVEZA` FOREIGN KEY (`idCerveza`) REFERENCES `cerveza` (`id`),
  CONSTRAINT `FKPAIS` FOREIGN KEY (`idPais`) REFERENCES `pais` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catálogo donde cada cerveza se produce.';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.compras_por_cliente
CREATE TABLE `compras_por_cliente` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idPersona` INT(11) NOT NULL COMMENT 'Llave primaria identificador único referencia de cada persona, se controla que sea cliente mediante PHP',
  `idCerveza` INT(11) NOT NULL COMMENT 'Llave primaria identificador único referencia de la cerveza comprada',
  `fecha` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se realiza la compra',
  `cantidad` INT(11) NOT NULL COMMENT 'Cantidad de cervezas compradas',
  PRIMARY KEY (`id`),
  INDEX `idPersona` (`idPersona`),
  INDEX `idCerveza` (`idCerveza`),
  CONSTRAINT `FK1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`id`),
  CONSTRAINT `FK2` FOREIGN KEY (`idCerveza`) REFERENCES `cerveza` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Compras hechas por cada cliente.';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla artesanos.bitacora
CREATE TABLE IF NOT EXISTS `bitacora` (
  `id` int(11) NOT NULL COMMENT 'Llave primaria identificador único de cada registro en bitácora', 
  `idUsuario` int(11) NOT NULL COMMENT 'Llave foránea referencia al Usuario que modifica un dato',
  `fecha_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se realia la modificación',
  `tabla_nombre` varchar(50) NOT NULL COMMENT 'Nombre de la tabla en la que se hace la edición',
  `columna_nombre` varchar(50) NOT NULL COMMENT 'Nombre de la columna donde se hace la edición',
  `valor_anterior` varchar(100) NOT NULL COMMENT 'Valor anterior del atributo editado',
  `valor_actual` varchar(100) NOT NULL COMMENT 'Valor con el que se edita el atributo',
  PRIMARY KEY (`id`),
  KEY `FK__persona` (`idUsuario`),
  CONSTRAINT `FK__persona` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = 'Registro e cambios en la base';

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla artesanos.parametros
CREATE TABLE IF NOT EXISTS `parametros` (
  `id` int(11) NOT NULL COMMENT 'Llave primaria identificador único de cada dato parametrizable',
  `nombre_parametro` varchar(50) NOT NULL COMMENT 'Nombre descriptivo de cada dato parametrizable',
  `usuario_ult_mod` int(11) NOT NULL COMMENT 'Usuario que modific[o por [ultima vez el parámetro',
  `fecha_ult_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se modificó',
  PRIMARY KEY (`id`),
  KEY `FK_usuario_last_mod` (`usuario_ult_mod`),
  CONSTRAINT `FK_usuario_last_mod` FOREIGN KEY (`usuario_ult_mod`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Parámetros del sistema.';

-- La exportación de datos fue deseleccionada.



-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
