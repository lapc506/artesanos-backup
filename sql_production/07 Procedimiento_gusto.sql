DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topCervezasXCliente`(IN `pUsuario` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Cervezas mas compradas por un solo cliente'
BEGIN
	SELECT c.marca, SUM(cantidad) as suma
	FROM persona AS p, cerveza AS c, compras_por_cliente AS cxc
	WHERE cxc.idCerveza = c.id AND cxc.idPersona = pUsuario
	GROUP BY idCerveza;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasPorNombre`(IN `pUsuario` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	select  c.marca, cxc.cantidad, cxc.fecha from cerveza as c, compras_por_cliente as cxc, persona as p
	where cxc.idCerveza = c.id and cxc.idPersona = p.id and p.usuario=pUsuario;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasPorNombreBack`(IN `pUsuario` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	SELECT c.marca, SUM(cantidad) as suma
	FROM persona AS p, cerveza AS c, compras_por_cliente AS cxc
	WHERE cxc.idCerveza = c.id AND cxc.idPersona = p.id AND p.usuario = pUsuario
	GROUP BY idCerveza;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getComprasCliente`(IN `pID` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	select  c.marca, cxc.cantidad, cxc.fecha from cerveza as c, compras_por_cliente as cxc, persona as p
	where cxc.idCerveza = c.id and cxc.idPersona = p.id and p.id=pID;
END//
DELIMITER ;