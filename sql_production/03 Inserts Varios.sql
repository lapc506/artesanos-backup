INSERT INTO  `persona` (`nombre`, `primerApellido`, `segundoApellido`, `numeroCedula`, `idDistrito`, `direccionExacta`, `usuario`, `contrasena`) 
	 VALUES
	('Roy', 'Barnes', 'Perez', 123456790, 65, 'Frente a la Iglesia', 'rbar', SHA1('hola')),
	('Andrés', 'Peña', 'Castillo', 123456791, 133, 'Del Bar 500 N', 'apena', SHA1('hola')),
	('Seidi', 'Acevedo', 'Lopez', 123456792, 120, 'Detrás de la plaza', 'sacev', SHA1('hola')),
	('Mayra', 'Benavides', 'Pineda', 123456793, 1, 'Super La puebla', 'mbena', SHA1('hola')),
	('Eugenia', 'Calderon', 'Quesada', 123456794, 45, 'Casa J10', 'ecald', SHA1('hola')),
	('Francisco', 'Corrales', 'Viquez', 123456795, 20, 'Casa B5', 'fcorr', SHA1('hola')),
	('Gilbert', 'Espinoza', 'Thompson', 123456796, 21, 'Boutique Estilo', 'gespi', SHA1('hola')),
	('Jesús', 'Salazar', 'Gutierrez', 123456797, 35, 'De la iglesia 45m O', 'jsala', SHA1('hola')),
	('Yolanda', 'López', 'Camacho', 123456798, 40, 'Rancho Camacho ', 'ylope', SHA1('hola')),
	('Lidieth', 'Monge', 'Moya', 123456799, 69, 'Del colegio 500m Sur', 'lmong', SHA1('hola'));

INSERT INTO `cliente` (`id`,`idPersona`) VALUES
	(0,2),(1,3),(2,4),(3,5),(4,6),(5,7),(6,8);
	
	
INSERT INTO `compras_por_cliente` (`id`,`idPersona`,`idCerveza`,`cantidad`) VALUES
	(NULL,2,20,3),
	(NULL,3,21,2),
	(NULL,4,22,5),
	(NULL,2,18,1),
	(NULL,5,20,1),
	(NULL,6,4,2),
	(NULL,4,19,3),
	(null,2,4,10),
	(null,5,19,9),
	(null,2,17,7),
	(null,3,20,2),
	(null,1,18,4);