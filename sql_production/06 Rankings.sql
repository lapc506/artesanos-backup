﻿-- Top cervezas compradas
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topCervezas`(IN `top` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Ranking de Cervezas más vendidas'
BEGIN
	SELECT c.marca, SUM(cantidad) as cantidadVendidas
	FROM compras_por_cliente AS cxc, cerveza AS c
	WHERE cxc.idCerveza = c.id 
	GROUP BY idCerveza
	ORDER BY cantidadVendidas DESC 
	LIMIT top; 
END//
DELIMITER ;

 
-- Top Clientes que más compran cervezas
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `topClientes`(IN `top` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Ranking de los clientes que más compran cervezas'
BEGIN
	SELECT p.usuario, p.nombre, p.primerApellido, SUM(cantidad) as cantidadCervezas
	FROM persona AS p, compras_por_cliente AS cxc
	WHERE cxc.idPersona = p.id
	GROUP BY idPersona
	ORDER BY cantidadCervezas DESC 
	LIMIT top; -- Top Parametrizable! 	
END//
DELIMITER ;