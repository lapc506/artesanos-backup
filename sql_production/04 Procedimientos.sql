-- Crear Cliente
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarCliente`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento agrega un cliente'
BEGIN
	INSERT INTO CLIENTE(id,idPersona,ultima_visita)VALUES(NULL,(SELECT MAX(id) FROM persona),DEFAULT);
END//
DELIMITER ;

-- Trigger Cliente
DELIMITER // 
CREATE DEFINER=`demo`@`localhost` TRIGGER `agregarCliente` AFTER INSERT ON `persona` FOR EACH ROW BEGIN
	INSERT INTO CLIENTE(id,idPersona,ultima_visita)VALUES(NULL,(SELECT MAX(id) FROM persona),DEFAULT);
END //
DELIMITER ;

-- Registrar cliente
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClienteById`(IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento se utiliza para el registro'
BEGIN
	SELECT *
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.usuario=pusuario and persona.contrasena=pcontrasena;
END//
DELIMITER ;

-- modificar persona
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `modificarUsuario`(IN `pnombre` VARCHAR(50), IN `papellido1` VARCHAR(50), IN `papellido2` VARCHAR(50), IN `pcedula` INT, IN `pidDistrito` INT, IN `pdireccion` VARCHAR(50), IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento modifica a un usuario'
BEGIN
	UPDATE PERSONA
	SET nombre=pnombre,primerApellido=papellido1,segundoApellido=papellido2,numeroCedula=pcedula,idDistrito=pidDistrito,direccionExacta=pdireccion,contrasena=pcontrasena
	WHERE usuario=pusuario;
END//
DELIMITER ;

-- Cantones 
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCanton`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona los cantones'
BEGIN
	SELECT id,idProvincia,descripcion FROM CANTON;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.agregarCanton
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarCanton`(IN `pprovincia` INT, IN `pcanton` VARCHAR(50))
    COMMENT 'catalogo canton'
BEGIN
	INSERT INTO CANTON(id,idProvincia,descripcion)VALUES(NULL,pprovincia,pcanton);
END//
DELIMITER ;


-- Distritos
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getDistrito`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona los distritos'
BEGIN
	SELECT id,idCanton,descripcion FROM DISTRITO;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.agregarDistrito
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarDistrito`(IN `pcanton` INT, IN `pdistrito` VARCHAR(50))
    COMMENT 'catalogo distrito'
BEGIN
	INSERT INTO DISTRITO(id,idCanton,descripcion)VALUES(NULL,pcanton,pdistrito);
END//
DELIMITER ;

-- Seleccionar todas las provincias
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getProvincia`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona las provincias'
BEGIN
	SELECT id,descripcion FROM PROVINCIA;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarProvincia`(IN `pprovincia` VARCHAR(50))
    COMMENT 'agregar a catalogo'
BEGIN
	INSERT INTO PROVINCIA(id,idPais,descripcion)VALUES(NULL,1,pprovincia);
END//
DELIMITER ;

-- Seleccionar todos los clientes
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClientes`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona los clientes'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on  persona.id = cliente.idPersona;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.getClienteByUser
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getClienteByUser`(IN `pusuario` VARCHAR(50))
BEGIN
	SELECT *
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.usuario=pusuario;
END//
DELIMITER ;

-- Búsqueda de personas por cédula
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCedulalike`(IN `pcedula` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona el numero de cedula'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.numeroCedula LIKE CONCAT('%', pcedula,'%');
END//
DELIMITER ;

-- Búsqueda por nombre 
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getNombrelike`(IN `nombre` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona por nombre'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.nombre LIKE CONCAT('%', nombre,'%');
END//
DELIMITER ;

-- Búsqueda por primer apellido
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPrimerApellidolike`(IN `pprimerApellido` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento selecciona por apellido1'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.primerApellido LIKE CONCAT('%', pprimerApellido,'%');
END//
DELIMITER ;

-- Búsqueda por segundo apellido
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getSegundoApellidolike`(IN `psegundoApellido` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento selecciona el apellido2'
BEGIN
	SELECT cliente.idPersona,persona.nombre,persona.primerApellido,persona.segundoApellido,persona.numeroCedula,persona.idDistrito,persona.direccionExacta,persona.usuario,persona.contrasena
	FROM cliente
	LEFT JOIN persona on persona.id = cliente.idPersona
	WHERE persona.segundoApellido LIKE CONCAT('%', psegundoApellido,'%');
END//
DELIMITER ;

-- Selecciona todas las personas con la información
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPersona`(IN `nombreUsuario` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento selecciona todos los datos de la persona'
BEGIN
	SELECT * FROM PERSONA
	WHERE usuario = nombreUsuario;
END//
DELIMITER ;

-- Seleccionar persona por ID
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getPersonabyId`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona la persona por id'
BEGIN
	SELECT * FROM PERSONA;
END//
DELIMITER ;

-- Selecciona los users
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getUsuario`(IN `pusuario` VARCHAR(50), IN `pcontrasena` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona los usuarios'
BEGIN
	SELECT id FROM PERSONA
	where usuario=pusuario and contrasena=pcontrasena;
END//
DELIMITER ;


-- Agregar cerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `crearCerveza`(IN `pidTipo` INT, IN `pidColor` INT, IN `pmarca` VARCHAR(50), IN `ptemperatura` INT, IN `ptiempo` INT, IN `pprecio` INT, IN `pIBU` INT, IN `pABV` INT, IN `pimagen` LONGBLOB)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento crea na cerveza'
BEGIN
	INSERT INTO CERVEZA(id,idTipo,idColor,marca,temperaturaFerm,tiempoFerm,precioColones,IBUamargura,ABValcohol,image)
	VALUES(NULL,pidTipo,pidColor,pmarca,ptemperatura,ptiempo,pprecio,pIBU,pABV,pimagen);
END//
DELIMITER ;

-- Desplegar Colores SRM
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getColores`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento retorna los colores'
BEGIN
	SELECT * FROM COLORSRM;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.getColorById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getColorById`(IN `pidColor` INT)
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM colorsrm
	LEFT JOIN cerveza on cerveza.idColor = colorsrm.id
	WHERE colorsrm.id=pidColor;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.agregarColor
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarColor`(IN `pcolor` VARCHAR(50), IN `PorcentajeInf` FLOAT, IN `PorcentajeSup` FLOAT)
BEGIN
	INSERT INTO COLORSRM(id,descripcion,porcentaje_inferior,porcentaje_superior)VALUES(NULL,pcolor,PorcentajeInf,PorcentajeSup);
END//
DELIMITER ;



-- Ver Familias
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getFamilias`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento retorna las familias'
BEGIN
	SELECT * FROM FAMILIACERVEZA;
END//
DELIMITER ;

-- Dumping structure for procedure artesanos.agregarFamilia
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarFamilia`(IN `pfamilia` VARCHAR(50), IN `pidTipoFerm` INT)
BEGIN
	INSERT INTO familiacerveza(id,idTipoFermentacion,descripcion)VALUES(NULL,pidTipoFerm,pfamilia);
END//
DELIMITER ;


-- Ver Tipos de cerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoCerveza`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento retorna los tipos de cerveza'
BEGIN
	SELECT * FROM TIPOCERVEZA;
END//
DELIMITER ;


-- Dumping structure for procedure artesanos.getTipoCervezaById
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoCervezaById`(IN `pidTipo` INT)
    COMMENT 'Este procedimiento selecciona dependiendo del ID'
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM tipocerveza
	LEFT JOIN cerveza on cerveza.idTipo = tipocerveza.id
	WHERE tipocerveza.id=pidTipo;
END//
DELIMITER ;


DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `agregarTipoCerveza`(IN `ptipo` VARCHAR(50), IN `pfamilia` INT, IN `pcuerpo` VARCHAR(50))
BEGIN
	INSERT INTO tipocerveza(id,idFamilia,descripcion,cuerpo)VALUES(NULL,pfamilia,ptipo,pcuerpo);
END//
DELIMITER ;

-- Imagenes
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getImagenes`()
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Proceso que retorna las imágenes'
BEGIN
	SELECT * FROM IMAGENES;
END//
DELIMITER ;

-- Buscar cervezas por ID
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCervezaById`(IN `pid` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	SELECT * FROM CERVEZA
	WHERE id = pid;
END//
DELIMITER ;

-- Buscar Cervezas por marca
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getCervezaByMarca`(IN `pmarca` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM cerveza
	WHERE cerveza.marca LIKE CONCAT('%', pmarca,'%');
END//
DELIMITER ;

-- Buscar Cervezas por color
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getColorById`(IN `pidColor` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM colorsrm
	LEFT JOIN cerveza on cerveza.idColor = colorsrm.id
	WHERE colorsrm.id=pidColor;
END//
DELIMITER ;

-- Buscar tipo de cerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoCervezaById`(IN `pidTipo` INT)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Este procedimiento selecciona dependiendo del ID'
BEGIN
	SELECT cerveza.id,cerveza.marca
	FROM tipocerveza
	LEFT JOIN cerveza on cerveza.idTipo = tipocerveza.id
	WHERE tipocerveza.id=pidTipo;
END//
DELIMITER ;
-- Editar cerveza
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `modificarCerveza`(IN `pid` INT, IN `pidTipo` INT, IN `pidColor` INT, IN `pmarca` VARCHAR(50), IN `ptemperatura` INT, IN `ptiempoFerm` INT, IN `pprecio` INT, IN `pIBU` INT, IN `pABV` INT, IN `pimage` LONGBLOB)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'este procedimiento modifica una cerveza'
BEGIN
	UPDATE CERVEZA
	SET idTipo=pidTipo,idColor=pidColor,marca=pmarca,temperaturaFerm=ptemperatura,tiempoFerm=ptiempoFerm,precioColones=pprecio,IBUamargura=pIBU,ABValcohol=pABV,image=pimage
	WHERE id=pid;
	commit;
END //
DELIMITER ;

-- Dumping structure for procedure artesanos.getTipoFerm
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getTipoFerm`()
BEGIN
	SELECT * FROM tipofermentacion;
END//
DELIMITER ;

-- Procedimientos bitácora
DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `bitacora`(IN `pUsuario` VARCHAR(50), IN `pTabla` VARCHAR(50), IN `pColumna` VARCHAR(50), IN `pValorAnt` VARCHAR(50), IN `pValorAct` VARCHAR(50))
BEGIN
	INSERT INTO bitacora(id,idUsuario,fecha_mod,tabla_nombre,columna_nombre,valor_anterior,valor_actual)
	VALUES(NULL,pUsuario,default,pTabla,pColumna,pValorAnt,pValorAct);
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`demo`@`localhost` PROCEDURE `getBitacoras`()
BEGIN
	SELECT * FROM BITACORA;
END//
DELIMITER ;
