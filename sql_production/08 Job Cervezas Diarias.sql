-- SELECT GROUP_CONCAT(msg SEPARATOR ';') FROM
-- 	(SELECT CONCAT_WS(',', marca,tipo,color) as msg FROM 
-- 		(SELECT cv.marca, tc.descripcion as tipo, col.descripcion as color FROM cerveza as cv, tipocerveza as tc, colorsrm as col
-- 			WHERE cv.idTipo = tc.id AND cv.idColor = col.id AND DATE(cv.fechaRegistro) = DATE(NOW())) as filtro
-- 	) as filtro2 into @mensaje;
-- INSERT INTO correos (id, msg, sent) VALUES (NULL, @mensaje, false);

CREATE TABLE `correos` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`msg` LONGTEXT NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`sent` TINYINT(4) NOT NULL,
	PRIMARY KEY (`id`)
)
COMMENT='Correos por enviar.'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


CREATE EVENT `crearCorreoCervezasNuevas`
	ON SCHEDULE
		EVERY 5 MINUTE STARTS '2016-06-08 23:03:00'
	ON COMPLETION PRESERVE
	ENABLE
	COMMENT 'Genera correos para ser enviados al administrador'
	DO BEGIN
	SELECT GROUP_CONCAT(msg SEPARATOR ';') FROM
	(SELECT CONCAT_WS(',', marca,tipo,color) as msg FROM 
		(SELECT cv.marca, tc.descripcion as tipo, col.descripcion as color FROM cerveza as cv, tipocerveza as tc, colorsrm as col
			WHERE cv.idTipo = tc.id AND cv.idColor = col.id AND DATE(cv.fechaRegistro) = DATE(NOW())) as filtro
	) as filtro2 into @mensaje;
	INSERT INTO correos (id, msg, sent) VALUES (NULL, @mensaje, false);
END