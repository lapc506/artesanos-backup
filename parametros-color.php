<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">CATALOGO <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>





<div class="container-fluid">
<h2 class="title-style-2"><b>Agregar Color</b><span class="title-under"></span></h2>
<form action="parametros-color.php" method="get">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
        <label style="width: 200px; display: block; float: left;" >Nombre del color:</label>
            <input name="color" id="color" class="form-control">
        </fieldset>
        <fieldset class="form-group">
            <label style="width: 200px; display: block; float: left;" >Porcentaje inferior:</label>
            <input id="porInf" name="porInf" type="number"  min="0" step= "0.1"  />
        </fieldset>
        <fieldset class="form-group">
            <label style="width: 200px; display: block; float: left;" >Porcentaje superior:</label>
            <input id="porSup" name="porSup" type="number"  min="0" step= "0.1"  />
        </fieldset>


        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit"  value ="Registrar color" />
        </fieldset>

    </div>
</form>
</div>
<?php


        if(isset($_GET['registrar'])&&$_GET['color']!=""&&$_GET['porInf']!=""&&$_GET['porSup']!=""){
            $db = new PDO(HOST,USER,PASS);
            $result = $db ->prepare("call agregarColor(:color,:porInf,:porSup);");
            $result -> execute(array(':color'=>$_GET['color'],':porInf'=>$_GET['porInf'],':porSup'=>$_GET['porSup']));
            echo '<script>alert("Color Registrado!");</script>';
        }


        ?>




    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>


    </body>
</html>
