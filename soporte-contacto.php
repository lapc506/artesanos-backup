<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>
    </head>
<body>
<?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Contáctenos<span class="title-under"></span></h1>
			<p class="page-description">Sus sugerencias son importantes<br>para nosotros</p>
		</div>
	</div>
<!-- <h2 class="title-style-1"><b>Buscar Cliente por Editar</b><span class="title-under"></span></h2> -->
<div class="container-fluid">
    <div class="table-responsive">
        <table class="table table-striped table-hover table-responsive">
            <thead>
                <th><span class="contact-icon"><i class="fa fa-users fa-lg"></i></span>
                    Miembro del equipo</th>
                <th>Ubicación</th>
                <th>Teléfono</th>
                <th>Correo</th>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">
                        <span class="contact-icon"><i class="fa fa-user fa-lg"></i></span>
                        Edwin Cen Xu
                    </th>
                    <td>
                        <span class="contact-icon"><i class="fa fa-map-marker fa-lg"></i></span>
                        San José, Costa Rica
                    </td>
                    <td>
                        <span class="contact-icon"><i class="fa fa-phone fa-lg"></i></span>
                        +506-8377-8712
                    </td>
                    <td>
                        <span class="contact-icon"> <i class="fa fa-envelope fa-lg"></i></span>
                        <a href="mailto:edwincx15@gmail.com" style="color: #9C6000">edwincx15@gmail.com</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <span class="contact-icon"><i class="fa fa-user fa-lg"></i></span>
                        Roy Barnes Perez
                    </th>
                    <td>
                        <span class="contact-icon"><i class="fa fa-map-marker fa-lg"></i></span>
                        Mercedes Norte, Heredia, Costa Rica
                    </td>
                    <td>
                        <span class="contact-icon"><i class="fa fa-phone fa-lg"></i></span>
                        +506-8661-5243
                    </td>
                    <td>
                        <span class="contact-icon"> <i class="fa fa-envelope fa-lg"></i></span>
                        <a href="mailto:rebp96@gmail.com" style="color: #9C6000">rebp96@gmail.com</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <span class="contact-icon"><i class="fa fa-user fa-lg"></i></span>
                        Andrés Peña Castillo
                    </th>
                    <td>
                        <span class="contact-icon"><i class="fa fa-map-marker fa-lg"></i></span>
                        San Francisco, Heredia, Costa Rica
                    </td>
                    <td>
                        <span class="contact-icon"><i class="fa fa-phone fa-lg"></i></span>
                        +506-8945-6736
                    </td>
                    <td>
                        <span class="contact-icon"> <i class="fa fa-envelope fa-lg"></i></span>
                        <a href="mailto:lapc506@hotmail.com" style="color: #9C6000">lapc506@hotmail.com</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<!--  Scripts
================================================== -->
<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Template main javascript -->
<script src="assets/js/main.js"></script>

<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>

    </body>
</html>
