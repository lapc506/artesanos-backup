<?php session_start();?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Edición de Cervezas<span class="title-under"></span></h1>
			<p class="page-description">No existe cerveza mala, existen malos gustos.</p>
		</div>
	</div>
    <div class="container-fluid">
        <form action="cerveza-edicion.php" method="post" enctype="multipart/form-data">
            <div class="fadeIn animated">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title-style-2"><b>Buscar cerveza para Editar</b><span class="title-under"></span></h2>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <label style="display: block;" >Familia de cerveza:</label>
                            <select name='bfamiliaCerveza' id="bfamiliaCerveza" class="form-control">
                                <option value="none" >Seleccione un la familia</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getFamilias();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Tipo de cerveza:</label>
                            <select name='btipoCerveza' id="btipoCerveza" class="form-control">
                                <option value="none" >Seleccione un tipo</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Color:</label>
                            <select name='bcolor' id="bcolor" class="form-control">
                                <option value="none" >Seleccione un color</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getColores();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Marca:</label>
                            <input id="bmarca" name="bmarca" type="text" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <input id="buscar" name="buscar" value ="Buscar"type="submit" class="btn btn-primary btn-block"/>
                        </fieldset>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <select id="listaCervezas" name="listaCervezas" type="listbox" size="12" class="form-control">
                           <?php // Check if image file is a actual image or fake image
                            if(isset($_POST["buscar"])) {
                                $db = new PDO(HOST,USER,PASS);
                                if($_POST['btipoCerveza']!='none'&&
                                   $_POST['bmarca']==''&&
                                   $_POST['bcolor']=='none'){
                                    $stmt = $db ->prepare('call getTipoCervezaById(:idTipo)');
                                    $stmt -> execute(array(':idTipo'=>$_POST['btipoCerveza']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }

                                if($_POST['btipoCerveza']=='none'&&
                                   $_POST['bmarca']==''&&
                                   $_POST['bcolor']!='none'){
                                    $stmt = $db ->prepare('call getColorById(:idColor)');
                                    $stmt -> execute(array(':idColor'=>$_POST['bcolor']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }
                                if($_POST['btipoCerveza']=='none'&&
                                   $_POST['bmarca']!=''&&
                                   $_POST['bcolor']=='none'){
                                    $stmt = $db ->prepare('call getCervezaByMarca(:marca)');
                                    $stmt -> execute(array(':marca'=>$_POST['bmarca']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }


                            }
                            ?>
                           </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <input id="editar" name="editar" value ="Editar"type="submit" class="btn btn-primary btn-block"/>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="fadeIn animated">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title-style-2"><b>Datos nuevos por ingresar...</b><span class="title-under"></span></h2>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <label style="display: block;">Seleccione una imagen:</label>
                            <img id="image" name="image" width="200" height="200">
                        </fieldset>
                        <fieldset class="form-group">
                            <input type="file" name="image2" id="image2" class="btn btn-default btn-block btn-file"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >Familia de cerveza:</label>
                            <select name='familiaCerveza' id="familiaCerveza" class="form-control">
                                <option value="none" >Seleccione un la familia</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getFamilias();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Tipo de cerveza:</label>
                            <select name='tipoCerveza' id="tipoCerveza" class="form-control">
                                <option value="none" >Seleccione un tipo: </option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >Color:</label>
                            <select name='color' id="color" class="form-control">
                                <option value="none" >Seleccione un color</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getColores();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <label style="display: block; " >Marca:</label>
                            <input id="marca" name="marca"  type="text" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >Temperatura(Celsius):</label>
                            <input id="temperatura" name="temperatura"  type="number" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >Tiempo de Fermentación(Dias):</label>
                            <input id="tiempoFerm" name="tiempoFerm" min="1"  type="number" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >Precio(colones):</label>
                            <input id="precio" name="precio"  type="text" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >IBU Amargura:</label>
                            <input id="IBUamargura" name="IBUamargura" min="0" type="number" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block; " >ABV alcohol:</label>
                            <input id="ABV" name="ABV" type="number" min="0" class="form-control"/>
                            <input id="id" name="id" type="hidden" value=""/>
                        </fieldset>
                        <fieldset class="form-group">
                            <input id="guardar" name="guardar" value ="Guardar"type="submit" class="btn btn-primary btn-block"/>
                        </fieldset>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php
    if(isset($_POST['editar'])){
        if(!empty($_POST['listaCervezas'])){
            $_SESSION['listaCervezas']=$_POST['listaCervezas'];
            $_SESSION['id'];
            $db = new PDO(HOST,USER,PASS);
            $stmt = $db ->prepare('call getCerveza();');
            $stmt -> execute();
            foreach($stmt as $i){

            if ($i['image']!=NULL&&$_POST['listaCervezas']==$i['id']){
                $_SESSION['id']=$i['id'];
            echo '<script>
            var image = document.getElementById("image");
            image.src= "data:image;base64,'.$i['image'].'";
            document.getElementById("marca").value="'.$i['marca'].'";
            document.getElementById("temperatura").value="'.$i['temperaturaFerm'].'";
            document.getElementById("tiempoFerm").value="'.$i['tiempoFerm'].'";
            document.getElementById("precio").value="'.$i['precioColones'].'";
            document.getElementById("IBUamargura").value="'.$i['IBUamargura'].'";
            document.getElementById("ABV").value="'.$i['ABValcohol'].'";


            </script>';}
                if($i['image']==NULL&&$_POST['listaCervezas']==$i['id']){
                    $_SESSION['id']=$i['id'];
                    echo '<script>

            document.getElementById("marca").value="'.$i['marca'].'";
            document.getElementById("temperatura").value="'.$i['temperaturaFerm'].'";
            document.getElementById("tiempoFerm").value="'.$i['tiempoFerm'].'";
            document.getElementById("precio").value="'.$i['precioColones'].'";
            document.getElementById("IBUamargura").value="'.$i['IBUamargura'].'";
            document.getElementById("ABV").value="'.$i['ABValcohol'].'";


            </script>';   }



            }





        }else{echo '<script>alert("Seleccione una cerveza");</script>';}

        }

    if(!empty($_POST['guardar'])){
        if($_POST['familiaCerveza']!='none'&&$_POST['tipoCerveza']!='none'&&$_POST['color']!='none'){

        $db2 = new PDO(HOST,USER,PASS);
        $bit = $db2 ->prepare("call getCervezaById(:id);");
        $bit -> execute(array(':id'=>$_SESSION['id']));
        foreach($bit as $r){
             $db3 = new PDO(HOST,USER,PASS);
            if ($_POST['marca']!=$r['marca']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','Marca',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['marca'],':valoract'=>$_POST['marca']));
            }
            if ($_POST['temperatura']!=$r['temperaturaFerm']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','Temperatura',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['temperaturaFerm'],':valoract'=>$_POST['temperatura']));
            }
            if ($_POST['tiempoFerm']!=$r['tiempoFerm']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','Tiempo de Fermentacion',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['tiempoFerm'],':valoract'=>$_POST['tiempoFerm']));
            }
            if ($_POST['precio']!=$r['precioColones']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','Precio',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['precioColones'],':valoract'=>$_POST['precio']));
            }
            if ($_POST['IBUamargura']!=$r['IBUamargura']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','IBU Amargura',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['IBUamargura'],':valoract'=>$_POST['IBUamargura']));
            }
            if ($_POST['ABV']!=$r['ABValcohol']){

            $bit2= $db3 ->prepare("call bitacora(:usuario,'Cerveza','ABV alcohol',:valorant,:valoract);");
            $bit2->execute(array(':usuario'=>$_COOKIE['empleado'],':valorant'=>$r['ABValcohol'],':valoract'=>$_POST['ABV']));
            }

        }


        $db = new PDO(HOST,USER,PASS);


        $potato = addslashes($_FILES["image2"]["tmp_name"]);
        $potato_name = addslashes($_FILES["image2"]["name"]);
        if (is_uploaded_file($_FILES['image2']['tmp_name'])){
            $potato = file_get_contents($potato);
        }
        $potato=base64_encode($potato);
        if (!is_uploaded_file($_FILES['image2']['tmp_name'])){
            $potato=NULL;

        }

        $stmt = $db ->prepare("call modificarCerveza(:id,:idTipo,:idColor,:marca,:temperatura,:tiempo,:precio,:IBU,:ABV,:image);");
        $stmt -> execute(array(':id'=> $_SESSION['id'],
                               ':idTipo'=> $_POST['tipoCerveza'],
                               ':idColor'=> $_POST['color'],
                               ':marca' => $_POST['marca'],
                               ':temperatura'=> $_POST['temperatura'],
                               ':tiempo'=> $_POST['tiempoFerm'],
                               ':precio'=> $_POST['precio'],
                               ':IBU'=>$_POST['IBUamargura'],
                               ':ABV'=>$_POST['ABV'],
                               ':image'=>$potato));

        echo '<script>alert("Cerveza modificada exitosamente!");location="cerveza-edicion.php";</script>';

        }
        else{
            echo'<script>alert("Faltan algunos datos");location="cerveza-edicion.php";</script>';}

    }
?>







    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
    $(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });
    $('#familiaCerveza').change(function(){
        var listFamilia = document.getElementById("familiaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("tipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>
</body>
</html>
