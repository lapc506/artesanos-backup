<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div id="homeCarousel" class="carousel slide carousel-home" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#homeCarousel" data-slide-to="1"></li>
        <li data-target="#homeCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="assets/images/slider/home-slider-1.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h2 class="carousel-title bounceInDown animated slow">Nuevas funcionalidades le sorprenderán</h2>
              <h4 class="carousel-subtitle bounceInUp animated slow ">Los clientes ahora también pueden iniciar sesión</h4>
              <!--
              <a href="#" class="btn btn-lg btn-secondary hidden-xs bounceInUp animated slow"
                 data-toggle="modal" data-target="#donateModal">DONATE NOW</a>
              -->
            </div>
          </div>
        </div> <!-- /.item -->

        <div class="item ">
          <img src="assets/images/slider/home-slider-2.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h2 class="carousel-title bounceInDown animated slow">Tenemos un gran catálogo a su disposición</h2>
              <h4 class="carousel-subtitle bounceInUp animated slow">Empiece a buscar y encontrará los gustos de sus clientes</h4>
              <!--
              <a href="#" class="btn btn-lg btn-secondary hidden-xs bounceInUp animated"
                 data-toggle="modal" data-target="#donateModal">DONATE NOW</a>
              -->
            </div> <!-- /.carousel-caption -->
          </div>
        </div> <!-- /.item -->

        <div class="item ">
          <img src="assets/images/slider/home-slider-3.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <h2 class="carousel-title bounceInDown animated slow" >Recuerde leer el manual de uso</h2>
              <h4 class="carousel-subtitle bounceInUp animated slow">Conozca a fondo su sistema</h4>
              <!--
              <a href="#" class="btn btn-lg btn-secondary hidden-xs bounceInUp animated"
                 data-toggle="modal" data-target="#donateModal">DONATE NOW</a>
              -->
            </div> <!-- /.carousel-caption -->
          </div>
        </div> <!-- /.item -->
      </div>

      <a class="left carousel-control" href="#homeCarousel" role="button" data-slide="prev">
        <span class="fa fa-angle-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="right carousel-control" href="#homeCarousel" role="button" data-slide="next">
        <span class="fa fa-angle-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->

    <div class="section-home about-us fadeIn animated">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-6">
                 <div class="about-us-col">
                     <h3 class="col-title"><a href="cerveza-busqueda.php"  >Cervezas</a></h3>
                        <div class="cause-details">
                            Busque información sobre su cerveza preferida!
                        </div>
                </div>
                </div>

                <div class="col-md-3 col-sm-6">
                  <div class="about-us-col">
                     <h3 class="col-title"><a href="ranking-cervezas.php"  >Top Cervezas</a></h3>
                        <div class="cause-details">
                            En la vida hay que tomarse las cosas con calma, menos la cerveza!
                        </div>
                </div>
                </div>

                <div class="col-md-3 col-sm-6">
                  <div class="about-us-col">
                     <h3 class="col-title"><a href="ranking-clientes.php"  >Top Clientes</a></h3>
                        <div class="cause-details">
                            Un hombre que miente sobre la cerveza se gana enemigos.
                        </div>
                </div>
                </div>

                <div class="col-md-3 col-sm-6">
                  <div class="about-us-col">
                     <h3 class="col-title"><a href="Manual%20de%20Usuario.pdf" >Manual de Uso</a></h3>
                        <div class="cause-details">
                            Conozca más sobre la página!
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>


    <!--  Scripts
    ================================================== -->
    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>
    </body>
</html>
