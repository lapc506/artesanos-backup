
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">


        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- PrettyPhoto -->
        <link rel="stylesheet" href="assets/css/prettyPhoto.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>


    </head>
    <body>
    <?php include('header.php') ?>
	<div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Registro de Personas<span class="title-under"></span></h1>
			<p class="page-description">Escoge entre empleado o Cliente.<br>Sólo para conocedores</p>
		</div>
	</div>

<div class="container-fluid">
<form method="POST" action="personas-registro-back.php">
    <!-- h2 class="title-style-1"><b>Registro</b><span class="title-under"></span></h2> -->
    <div class="col-md-4 col-sm-6 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Nombre:</label>
            <input id="nombre" name="nombre" type="text" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Primer Apellido:</label>
            <input id="apellido1" name="apellido1" type="text" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Segundo Apellido:</label>
            <input id="apellido2" name="apellido2" type="text" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Cédula:</label>
            <input id="cedula" name="cedula" type="text" maxlength="10" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Provincia:</label>
            <select required name='provincia' id="provincia" class="form-control" >
                <option value="none" >Seleccione una provincia</option>
                <?php
                    $db = new PDO(HOST,USER,PASS);
                    $t = $db->prepare('call getProvincia();');
                    $t->execute();
                    //$t->setFetchMode(PDO::FETCH_ASSOC);
                    foreach($t as $provincia){
                        echo '<option  value="'.$provincia['id'].'">'.$provincia['descripcion'].'</option>';
                    }
                ?>
            </select>
        </fieldset>
    </div>
    <div class="col-md-4 col-sm-6 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Cantón:</label>
            <select required name='canton' id="canton" class="form-control" >
                <option value="none" >Seleccione un cantón</option>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Distrito:</label>
            <select required name='distrito' id="distrito" class="form-control" >
                <option value="none" >Seleccione un distrito</option>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Otra Dirección:</label>
            <textarea id="direccion" name="direccion" rows="6" class="form-control"></textarea>
        </fieldset>
        <fieldset class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="checkbox" name="cliente" id="cliente" value="cliente" />  <label>Cliente</label>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="checkbox" name="empleado" id="empleado" value="empleado"/>  <label>Empleado</label>
            </div>
        </fieldset>
    </div>
    <div class="col-md-4 col-sm-12 col-form">
        <fieldset class="form-group">
            <label style="display: block; " >Usuario:</label>
            <input id="user" name="user" type="text" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Contraseña:</label>
            <input id="password" name="password" type="password" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block; " >Repetir Contraseña:</label>
            <input id="REpassword" name="REpassword" type="password" class="form-control" >
        </fieldset>
        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit" value="Registrarse" class="btn btn-primary btn-block"/>
        </fieldset>
    </div>
</form>
</div>
<!--  Scripts
================================================== -->

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>

<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>

<!-- Template main javascript -->
<script src="assets/js/main.js"></script>

<script>
    $(document).ready(function () {
  //called when key is pressed in textbox
        $("#cedula").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });

    $('#provincia').change(function(){
        var listProvincias = document.getElementById("provincia")
        var provincia = listProvincias.options[listProvincias.selectedIndex].value;
        var select = document.getElementById("canton");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");
        o.text="Seleccione un cantón";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            //$db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getCanton();');
            $t->execute();
            foreach($t as $canton){?>
                if (provincia == <?php echo $canton['idProvincia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
        var select2 = document.getElementById("distrito");
        var d = document.createElement("OPTION");
        d.text="Seleccione un distrito";
        d.value="none";
        select2.length = 0;
        select2.options.add(d);
    });

    $('#canton').change(function(){
        var listCantones = document.getElementById("canton")
        var canton = listCantones.options[listCantones.selectedIndex].value;
        var select = document.getElementById("distrito");
        var o = document.createElement("OPTION");
        o.text="Seleccione un distrito";
        o.value="none";
        select.length = 0;
        select.options.add(o);
        <?php
            //$db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getDistrito();');
            $t->execute();
            foreach($t as $canton){?>
                if (canton == <?php echo $canton['idCanton'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$canton["descripcion"]."'";?>;
				    o.value = <?php echo $canton["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>
    });

        $("#cliente").change(function (){
            $("#empleado").prop("checked", false);});

        $("#empleado").change(function (){
            $("#cliente").prop("checked", false);});

        </script>
    </body>
</html>
