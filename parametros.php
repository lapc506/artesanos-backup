<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">CATALOGO <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>



<div class="container-fluid">
        <h2 class="title-style-2"><b>Catálogos del sistema</b><span class="title-under"></span></h2>
<form action="parametros.php" method="get">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
            <select name="parametro" id="parametro" class="form-control">
                <option value="none">Seleccione el parametro</option>
                <option value="1">Agregar Provincia</option>
                <option value="2">Agregar Cantón</option>
                <option value="3">Agregar Distrito</option>
                <option value="4">Agregar Color</option>
                <option value="5">Agregar Familia de Cerveza</option>
                <option value="6">Agregar Tipo de Cerveza</option>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <input id="entrar" name="entrar" type="submit"  value ="Entrar" />
        </fieldset>
    </div>
</form>
</div>
<?php
        if(isset($_GET['entrar'])&&$_GET['parametro']=='1'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-provincia.php";</script>';
        }
        if(isset($_GET['entrar'])&&$_GET['parametro']=='2'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-canton.php";</script>';
        }
        if(isset($_GET['entrar'])&&$_GET['parametro']=='3'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-distrito.php";</script>';
        }
        if(isset($_GET['entrar'])&&$_GET['parametro']=='4'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-color.php";</script>';
        }
        if(isset($_GET['entrar'])&&$_GET['parametro']=='5'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-familia.php";</script>';
        }
        if(isset($_GET['entrar'])&&$_GET['parametro']=='6'){
            //header('Location: http://localhost/'.$_GET['parametro']);
            echo '<script>location="parametros-tipocerveza.php";</script>';
        }

        ?>




    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>

    </body>
</html>
