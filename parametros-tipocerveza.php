<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Edición de Parámetros del Sistema<span class="title-under"></span></h1>
			<p class="page-description">Defina las mejoras condiciones para su negocio</p>
		</div>
	</div>
    <div class="container-fluid">
        <h2 class="title-style-1"><b>Agregar un tipo de cerveza</b><span class="title-under"></span></h2>

<form action="parametros-tipocerveza.php" method="get">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
            <label style="display: block;" >Familia:</label>
            <select name="familia" id="familia" >
                <option value="none" >Seleccione una familia</option>
                <?php
                    $db = new PDO(HOST,USER,PASS);
                    $t = $db->prepare("call getFamilias();");
                    $t->execute();
                    //$t->setFetchMode(PDO::FETCH_ASSOC);
                    foreach($t as $familia){
                        echo '<option value="'.$familia['id'].'">'.$familia['descripcion'].'</option>';
                    }
                ?>
            </select>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Cuerpo:</label>
            <select name="cuerpo" id="cuerpo" >
                <option value="none" >Seleccione un cuerpo</option>
                <option value="Pesada" >Pesada</option>
                <option value="Medio" >Medio</option>
                <option value="Ligera" >Ligera</option>
            </select>
        </fieldset>
    </div>
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
            <label style="display: block;" >Nombre del tipo:</label>
            <input name="tipo" id="tipo" class="form-control">
        </fieldset>
        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit"  value ="Registrar Tipo" class="btn btn-primary btn-block"/>
        </fieldset>
    </div>
</form>
</div>
<?php
        if(isset($_GET['registrar'])){
            if($_GET['tipo']!=""&&$_GET['familia']!='none'&&$_GET['cuerpo']!='none'){
            $db = new PDO(HOST,USER,PASS);
            $result = $db ->prepare("call agregarTipoCerveza(:tipo,:familia,:cuerpo);");
            $result -> execute(array(':tipo'=>$_GET['tipo'],':familia'=>$_GET['familia'],':cuerpo'=>$_GET['cuerpo']));
            echo '<script>alert("Tipo de Cerveza Registrado!");</script>';}else{ echo '<script>alert("Faltan Datos");</script>';}
        }
?>

    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>


    </body>
</html>
