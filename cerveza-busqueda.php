<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Búsqueda de Cervezas<span class="title-under"></span></h1>
			<p class="page-description">Dar la vuelta al mundo en una Jarra</p>
		</div>
	</div>



<div class="container-fluid">
    <form action="cerveza-busqueda.php" method="post" enctype="multipart/form-data">
    <div class="fadeIn animated">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <h2 class="title-style-1"><b>Búsqueda de Cervezas</b><span class="title-under"></span></h2>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <fieldset class="form-group">
                        <label style="display: block;" >Familia de cerveza:</label>
                        <select name='bfamiliaCerveza' id="bfamiliaCerveza" class="form-control">
                            <option value="none" >Seleccione un la familia</option>
                            <?php
                                $db = new PDO(HOST,USER,PASS);
                                $result = $db->prepare('call getFamilias();');
                                $result->execute();
                                foreach($result as $i){
                                    echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                }?>
                        </select>
                </fieldset>
                <fieldset class="form-group">
                        <label style="display: block;" >Tipo de cerveza:</label>
                        <select name='btipoCerveza' id="btipoCerveza" class="form-control">
                            <option value="none" >Seleccione un tipo</option>
                        </select>
                </fieldset>
                <fieldset class="form-group">
                        <label style="display: block;" >Color:</label>
                        <select name='bcolor' id="bcolor" class="form-control">
                            <option value="none" >Seleccione un color</option>
                            <?php
                                $db = new PDO(HOST,USER,PASS);
                                $result = $db->prepare('call getColores();');
                                $result->execute();
                                foreach($result as $i){
                                    echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                }?>
                        </select>
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >Marca:</label>
                    <input id="bmarca" name="bmarca"  type="text" class="form-control"/>
                </fieldset>
                <fieldset class="form-group">
                    <input id="buscar" name="buscar" value ="Buscar"type="submit" class="btn btn-primary btn-block"/>
                </fieldset>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <fieldset class="form-group">
                    <select id="listaCervezas" name="listaCervezas" type="listbox" size="12" class="form-control">
                   <?php
            // Check if image file is a actual image or fake image
                    if(isset($_POST["buscar"])) {
                        $db = new PDO(HOST,USER,PASS);
                        if($_POST['btipoCerveza']!='none'&&
                           $_POST['bmarca']==''&&
                           $_POST['bcolor']=='none'){
                            $stmt = $db ->prepare('call getTipoCervezaById(:idTipo)');
                            $stmt -> execute(array(':idTipo'=>$_POST['btipoCerveza']));
                            foreach($stmt as $i){
                            echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                        }
                        }

                        if($_POST['btipoCerveza']=='none'&&
                           $_POST['bmarca']==''&&
                           $_POST['bcolor']!='none'){
                            $stmt = $db ->prepare('call getColorById(:idColor)');
                            $stmt -> execute(array(':idColor'=>$_POST['bcolor']));
                            foreach($stmt as $i){
                            echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                        }
                        }
                        if($_POST['btipoCerveza']=='none'&&
                           $_POST['bmarca']!=''&&
                           $_POST['bcolor']=='none'){
                            $stmt = $db ->prepare('call getCervezaByMarca(:marca)');
                            $stmt -> execute(array(':marca'=>$_POST['bmarca']));
                            foreach($stmt as $i){
                                echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                            }
                        }

                    }
                    ?>
                   </select>
                </fieldset>
                <fieldset class="form-group">
                    <input id="ver" name="ver" value="Mostrar más Información >>>"type="submit" class="btn btn-primary btn-block"/>
                </fieldset>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 fadeIn animated col-form">
            <h2 class="title-style-1"><b>Información de la Cerveza<span class="title-under"></span></b></h2>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <fieldset class="form-group">
                    <label style="display: block;" >Fotografía:</label>
                    <img id="image" width="100%" height="200">
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >Marca:</label>
                    <input id="marca" name="marca"  type="text" class="form-control" READONLY/>
                </fieldset>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <fieldset class="form-group" >
                    <label style="display: block;" >Temperatura(Celsius):</label>
                    <input id="temperatura" name="temperatura"  type="text" class="form-control" READONLY />
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >Tiempo de Fermentación (en días):</label>
                    <input id="tiempoFerm" name="tiempoFerm"  type="text" class="form-control" READONLY />
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >Precio (colones):</label>
                    <input id="precio" name="precio"  type="text" class="form-control" READONLY />
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >IBU (índice de Amargura):</label>
                    <input id="IBUAmargura" name="IBUAmargura" type="text" class="form-control" READONLY/>
                </fieldset>
                <fieldset class="form-group">
                    <label style="display: block;" >ABV (Alcohol por Volumen):</label>
                    <input id="ABV" name="ABV" type="text" class="form-control" READONLY />
                </fieldset>
            </div>
        </div>
    </div>
    </form>
</div> <!-- Final del container-fluid -->

<?php
    if(isset($_POST["ver"]))
    {
        $db = new PDO(HOST,USER,PASS);
        $stmt = $db ->prepare('call getCervezaById(:pid);');
        $stmt -> execute(array(':pid'=>$_POST['listaCervezas']));
        foreach($stmt as $i)
        {
            if ($i['image']!=NULL&&$_POST['listaCervezas']==$i['id']){

            echo '<script>
            var image = document.getElementById("image");
            image.src= "data:image;base64,'.$i['image'].'";
            document.getElementById("marca").value="'.$i['marca'].'";
            document.getElementById("temperatura").value="'.$i['temperaturaFerm'].'";
            document.getElementById("tiempoFerm").value="'.$i['tiempoFerm'].'";
            document.getElementById("precio").value="'.$i['precioColones'].'";
            document.getElementById("IBUAmargura").value="'.$i['IBUamargura'].'";
            document.getElementById("ABV").value="'.$i['ABValcohol'].'";


            </script>';}
                if($i['image']==NULL&&$_POST['listaCervezas']==$i['id']){

                    echo '<script>

            document.getElementById("marca").value="'.$i['marca'].'";
            document.getElementById("temperatura").value="'.$i['temperaturaFerm'].'";
            document.getElementById("tiempoFerm").value="'.$i['tiempoFerm'].'";
            document.getElementById("precio").value="'.$i['precioColones'].'";
            document.getElementById("IBUAmargura").value="'.$i['IBUamargura'].'";
            document.getElementById("ABV").value="'.$i['ABValcohol'].'";


            </script>';   }

        }
    }
?>
<!--  Scripts
================================================== -->
<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Template main javascript -->
<script src="assets/js/main.js"></script>
<script>$(document).ready(function () {
//called when key is pressed in textbox
    $("#precio").keypress(function (e) {
 //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
});

    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>
</body>
</html>
