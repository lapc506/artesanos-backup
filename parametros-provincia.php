<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Edición de Parámetros del Sistema<span class="title-under"></span></h1>
			<p class="page-description">Defina las mejoras condiciones para su negocio</p>
		</div>
	</div>

<div class="container-fluid">
<h2 class="title-style-1"><b>Agregar Provincia</b><span class="title-under"></span></h2>
<form action="parametros-provincia.php" method="get">
    <div class="col-md-12 col-sm-12 fadeIn animated col-form">
        <fieldset class="form-group">
        <label style="display: block; " >Nombre de la provincia:</label>
            <input name="provincia" id="provincia" class="form-control">
        </fieldset>
        <fieldset class="form-group">
            <input id="registrar" name="registrar" type="submit" class="form-control" value ="Registrar provincia" />
        </fieldset>
    </div>
</form>
</div>
<?php


        if(isset($_GET['registrar'])&&$_GET['provincia']!=""){
            $db = new PDO(HOST,USER,PASS);
            $result = $db ->prepare("call agregarProvincia(:provincia);");
            $result -> execute(array(':provincia'=>$_GET['provincia']));
            echo '<script>alert("Provincia Registrada!");</script>';
        }


        ?>




    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>


    </body>
</html>
