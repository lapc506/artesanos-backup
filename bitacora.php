<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>
    </head>
<body>
<?php include('header.php'); ?>
    <div class="page-heading text-center">
		<div class="container zoomIn animated">
			<h1 class="page-title">Bitácora<span class="title-under"></span></h1>
			<p class="page-description">Registro de cambios</p>
		</div>
	</div>
<!-- <h2 class="title-style-1"><b>Buscar Cliente por Editar</b><span class="title-under"></span></h2> -->
<div class="container-fluid">
    <div class="table-responsive">
        <table class="table table-striped table-hover table-responsive">
            <thead>
                <th><font size="3"><b>Usuario</b></font></th>
                <th>Tipo</th>
                <th>Campo</th>
                <th>Valor Anterior</th>
                <th>Valor Actual</th>
                <th>Fecha de modificación</th>
            </thead>
            <tbody>
            <?php
            $db = new PDO(HOST,USER,PASS);
            $stmt = $db ->prepare("call getBitacoras();");
            $stmt -> execute();
            foreach($stmt as $i){
            echo '<tr>
                    <th scope="row">'.$i['idUsuario'].'</th>
                    <td>'.$i['tabla_nombre'].'</td>
                    <td>'.$i['columna_nombre'].'</td>
                    <td>'.$i['valor_anterior'].'</td>
                    <td>'.$i['valor_actual'].'</td>
                    <td>'.$i['fecha_mod'].'</td>
                 </tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<!--  Scripts
================================================== -->
<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>
<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carouseljavascript file -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Template main javascript -->
<script src="assets/js/main.js"></script>

<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>

    </body>
</html>
