<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">Registro de Cervezas<span class="title-under"></span></h1>
			<p class="page-description">Siempre hay espacio para una más!</p>

		</div>

	</div>

<?php
// Check if image file is a actual image or fake image
if(isset($_POST["registrar"])) {
    if(
       $_POST['tipoCerveza']!='none'&&
       $_POST['temperatura']!=''&&
       $_POST['marca']!=''&&
      $_POST['tiempoFerm']!=''&&
      $_POST['precio']!=''&&
      $_POST['color']!=''&&
       $_POST['IBUAmargura']!=''&&
      $_POST['ABV']!=''){
    $db = new PDO(HOST,USER,PASS);
    $potato = addslashes($_FILES["image"]["tmp_name"]);
    $potato_name = addslashes($_FILES["image"]["name"]);
    if (is_uploaded_file($_FILES['image']['tmp_name'])){
        $potato = file_get_contents($potato);
    }
    $potato=base64_encode($potato);
    if (!is_uploaded_file($_FILES['image']['tmp_name'])){
        $potato=NULL;
    }
    $stmt = $db ->prepare("call crearCerveza(:idTipo,:color,:marca,:temperatura,:tiempo,:precio,:IBU,:ABV,:image);");
    $stmt -> execute(array(':idTipo'=>$_POST['tipoCerveza'],
                           ':color'=>$_POST['color'],
                           ':marca'=>$_POST['marca'],
                           ':temperatura'=>$_POST['temperatura'],
                           ':tiempo'=>$_POST['tiempoFerm'],
                           ':precio'=>$_POST['precio'],
                           ':IBU'=>$_POST['IBUAmargura'],
                           ':ABV'=>$_POST['ABV'],
                           ':image'=>$potato));


        echo '<script>alert("Se registro exitosamente!");</script>';
    }else{ echo '<script>alert("Por favor llenar todos los datos");</script>';}


}
        if(isset($_POST['mostrar'])){
            $db = new PDO(HOST,USER,PASS);

            $result = $db ->prepare("call getImagenes();");
            $result -> execute();

            foreach($result as $img){
            echo '<img height="300" width="300" src="data:image;base64,'.$img['imagen'].'"/>';
            }
        }
        ?>
<div class="container-fluid">
    <form action="cerveza-registro.php" method="post" enctype="multipart/form-data">
    <div class="fadeIn animated">
        <div class="col-md-4 col-sm-6 col-xs-12">
        <!-- <h2 class="title-style-2"><b>Registro de Cervezas</b><span class="title-under"></span></h2> -->
            <fieldset class="form-group">
                <label style="display: block; " >Seleccione una imagen:</label>
                <input type="file"  name="image" id="image" class="btn btn-default btn-block btn-file"/>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >Familia de cerveza:</label>
                <select name='familiaCerveza' id="familiaCerveza" class="form-control">
                    <option value="none" >Seleccione un la familia</option>
                    <?php
                        $db = new PDO(HOST,USER,PASS);
                        $result = $db->prepare('call getFamilias();');
                        $result->execute();
                        foreach($result as $i){
                            echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                        }?>
                </select>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >Tipo de cerveza:</label>
                <select name='tipoCerveza' id="tipoCerveza" class="form-control">
                    <option value="none" >Seleccione un tipo</option>
                </select>
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >Color:</label>
                <select name='color' id="color" class="form-control">
                    <option value="none" >Seleccione un color</option>
                    <?php
                        $db = new PDO(HOST,USER,PASS);
                        $result = $db->prepare('call getColores();');
                        $result->execute();
                        foreach($result as $i){
                            echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                        }?>
                </select>
            </fieldset>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <fieldset class="form-group">
                <label style="display: block; " >Marca:</label>
                <input id="marca" name="marca"  type="text" class="form-control">
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >Temperatura (Celsius):</label>
                <input id="temperatura" name="temperatura" type="number" class="form-control">
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >Tiempo de Fermentación (en días):</label>
                <input id="tiempoFerm" name="tiempoFerm" min="1"  type="number" class="form-control">
            </fieldset>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <fieldset class="form-group">
                <label style="display: block; " >Precio (colones):</label>
                <input id="precio" name="precio"  type="text" class="form-control">
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >IBU Amargura:</label>
                <input id="IBUAmargura" name="IBUAmargura" type="number" min="0" class="form-control">
            </fieldset>
            <fieldset class="form-group">
                <label style="display: block; " >ABV alcohol:</label>
                <input id="ABV" name="ABV" type="number" min="0" class="form-control"/>
            </fieldset>
            <fieldset class="form-group">
                <input id="registrar" name="registrar" value ="Registrar"type="submit" class="btn btn-primary btn-block"/>
            </fieldset>
        </div>
    </div>
    </form>
</div>













    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });


    $('#familiaCerveza').change(function(){
        var listFamilia = document.getElementById("familiaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("tipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>
    </body>
</html>
