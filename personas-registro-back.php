<?php
include ("private.php");
if(!empty($_POST['registrar'])
   &&!empty($_POST['nombre'])
   &&!empty($_POST['apellido1'])
   &&!empty($_POST['apellido2'])
   &&!empty($_POST['cedula'])
   &&!empty($_POST['direccion'])
   &&!empty($_POST['user'])
   &&!empty($_POST['password'])
   &&$_POST['provincia']!='none'
   &&$_POST['canton']!='none'
   &&$_POST['distrito']!='none')
{
    if($_POST['password']==$_POST['REpassword']){
        $db = new PDO(HOST,USER,PASS);
        if ($db==NULL){
            echo'<script>alert("no se conecto");</script>';
        }
        $stmt = $db ->prepare("call crearPersona(:nombre,:primerApellido,:segundoApellido,:cedula,:idDistrito,:direccion,:usuario,SHA1(:contrasena));");
        $stmt -> execute(
            array(':nombre'=> $_POST['nombre'],
                    ':primerApellido'=> $_POST['apellido1'],
                    ':segundoApellido'=> $_POST['apellido2'],
                    ':cedula' => $_POST['cedula'],
                   ':idDistrito'=> $_POST['distrito'],
                   ':direccion'=> $_POST['direccion'],
                   ':usuario'=>$_POST['user'],
                   ':contrasena'=>$_POST['password'])
        );
        if(!empty($_POST['cliente'])){
            $stmt = $db -> prepare("call agregarCliente();");
            $stmt -> execute();
        }
        echo '<script> alert("Usuario registrado exitosamente!"); location="index.php"; </script>';
    } else {
        echo'<script>alert("Contraseñas no coinciden");location="personas-registro.php";</script>';
    }
} else {
    echo'<script>alert("Faltan Datos");location="personas-registro.php";</script>';
}
?>
