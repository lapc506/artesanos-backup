<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="navbar-top">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <?php
            include ("private.php");
            if(empty($_COOKIE['cliente'])&&empty($_COOKIE['empleado'])){
                // Barra de inicio de sesión:
                echo
                '<form method="GET" action="header.php" id="formregistro">
                    <span class="glyphicon glyphicon-user"></span>
                    <input type="text" id="user" name="user" placeholder="Usuario" style="color:#000000;">
                    <span class="glyphicon glyphicon-lock"></span>
                    <input type="password" name="password" placeholder="Contraseña" style="color:#000000;">
                    <span class="glyphicon glyphicon-log-in"></span>
                    <input type="submit" value="Entrar" name="enter" style="color:#000000;">
                </form>';

                if(!empty($_GET['enter'])) {
                    if(!empty($_GET['user'])&&!empty($_GET['password'])){
                      $db = new PDO(HOST,USER,PASS);
                      $t = $db->prepare('call getClienteById(:user,SHA1(:password));');
                      $t->execute(array(':user'=>$_GET['user'],':password'=>$_GET['password']));
                      $row1 = $t -> rowCount();
                      if ($row1>0) {
                            setcookie("cliente",$_GET['user'],time()+(86400*7));
                            header('Location: http://localhost/index.php');
                            //echo '<script>location="index.php";</script>';
                      }
                      $t = $db->prepare('call getUsuario(:user,SHA1(:password));');
                      $t->execute(array(':user'=>$_GET['user'],':password'=>$_GET['password']));

                      $row = $t -> rowCount();
                      if ($row>0&&empty($_COOKIE['cliente'])&&$row1==0) {
                            setcookie("empleado",$_GET['user'],time()+(86400*7));
                            header('Location: http://localhost/index.php');
                            //echo '<script>location="index.php";</script>';
                      }


                        else {
                        echo '<script>alert("Usuario o Contrasena incorrecta");location="index.php";</script>';
                      }
                    } else {
                      echo '<script>alert("Usuario o Contrasena invalida");location="index.php";</script>';
                    }
                }
            } else {
                $user;
                if (!empty($_COOKIE['cliente'])){
                    $user='cliente';
                }
                if (!empty($_COOKIE['empleado'])){
                    $user='empleado';
                }
                echo '<form action="header.php" method="get">
                  <div><font size="4">
                    Bienvenido '.$_COOKIE[$user].'!</font>
                    <input type="submit" name="logout" value="Cerrar Sesión" class="btn btn-default-xs"/>
                  </div>
                </form>';
                if(!empty($_GET['logout'])){
                  setcookie($user, "", time()-3600);
                  header('Location: http://localhost/index.php');
                  //header('Location: http://localhost/index.php');
                }
            }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar-main">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed"
        data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <table class="navbar-brand" style="margin-top: 5px;"><a href="index.php">
        <tr>
        <td>
            <img src="assets/images/icons/beer-32.png" alt="">
        </td>
        <td>
            <h3 class="navbar-brand-title">ArtesanosDB</h3>
        </td>
        </tr>
        </a></table>

    </div>
    <div id="navbar" class="navbar-collapse collapse pull-right">
      <ul class="nav navbar-nav">
        <li><a  href="index.php">Inicio</a></li>
          <?php
          if (!empty($_COOKIE['empleado'])||!empty($_COOKIE['cliente'])){
              echo '<li><a  href="personas-edicion.php">Cuenta</a></li>';}

            if (!empty($_COOKIE['empleado'])&&empty($_COOKIE['cliente'])){
              echo '
        <li class="has-child"><a href="#">Empleados</a>
          <ul class="submenu">
            <li class="submenu-item"><a href="personas-registro.php">Registrar nueva Persona...</a></li>
            <li class="submenu-item"><a href="bitacora.php">Bitácora</a></li>
            <li class="submenu-item"><a href="parametros.php">Catálogo del Sistema</a></li>
          </ul>
        </li>';}?>
        <li class="has-child"><a href="#">Cervezas</a>
          <ul class="submenu">
            <li class="submenu-item"><a href="cerveza-busqueda.php">Búsqueda</a></li>
            <?php
            if (!empty($_COOKIE['empleado'])&&empty($_COOKIE['cliente'])){
              echo '
            <li class="submenu-item"><a href="cerveza-registro.php">Registrar nueva Cerveza</a></li>
            <li class="submenu-item"><a href="cerveza-edicion.php">Editar Cerveza</a></li>';}?>
          </ul>
        </li>
        <li class="has-child"><a href="#">Clientes</a>
          <ul class="submenu">
              <?php
              if (empty($_COOKIE['empleado'])&&!empty($_COOKIE['cliente'])){
                  echo'
                  <li class="submenu-item"><a href="gustos-cliente.php">Gustos</a></li>';

              }

              if (!empty($_COOKIE['empleado'])&&empty($_COOKIE['cliente'])){
              echo '
              <li class="submenu-item"><a href="gustos.php">Gustos</a></li>
              <li class="submenu-item"><a href="compras.php">Registrar Compra</a></li>
              <li class="submenu-item"><a href="cliente-editar.php">Editar Cliente</a></li>

              ';}?>
          </ul>
        </li>
        <li class="has-child"><a href="#">Rankings</a>
          <ul class="submenu">
            <li class="submenu-item"><a href="ranking-cervezas.php">Top Cervezas</a></li>
            <li class="submenu-item"><a href="ranking-clientes.php">Top Clientes</a></li>
          </ul>
        </li>
        <li class="has-child"><a href="#">Soporte</a>
          <ul class="submenu">
            <li class="submenu-item"><a href="Manual%20de%20Usuario.pdf">Manual de Uso</a></li>
            <li class="submenu-item"><a href="soporte-contacto.php">Contacto</a></li>
          </ul>
        </li>
      </ul>
    </div> <!-- /#navbar -->
  </div> <!-- /.container -->
  </div> <!-- /.navbar-main -->
  </nav>
</header>
