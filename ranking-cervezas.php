<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">Top Cervezas!<span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>
<div class="col-md-3 col-sm-3 col-xs-12 fadeIn animated col-form">
    <form method="get" action="ranking-cervezas.php">
        <h2 class="title-style-2"><b>Defina un límite</b><span class="title-under"></span></h2>
        <fieldset class="form-group">
            <input id="top" name="top" type="number" min="1" class="form-control">
            <input id="consultar" name="consultar" type="submit" value="Consultar" class="btn btn-primary btn-block">
        </fieldset>
   </form>
</div>
<div class="col-md-9 col-sm-6 col-xs-12 fadeIn animated">
    <div class="panel-primary">
        <div class="panel-heading"><h5 style="display: block;" >Top Cervezas compradas</h5></div>
        <div class="panel-body" style="min-height:200px;max-height:200px;overflow-y:scroll;">
            <table data-toggle="table" id="table-style" data-row-style="rowStyle"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-single-select="true" data-click-to-select="true" data-maintain-selected="true" class="table table-striped table-hover table-responsive">
              <thead>
                <tr>
                  <th>Cerveza</th>
                  <th>Cantidad</th>
                </tr>
              </thead>
                <tbody>
                <?php
                    if(!empty($_GET['consultar'])&&!empty($_GET['top']))
                    {
                        $db = new PDO(HOST,USER,PASS);
                        $result = $db->prepare('call topCervezas(:top);');
                        $result->execute(array(':top'=>$_GET['top']));

                        foreach($result as $i){
                            echo '
                                <tr>

                                    <td>'.$i['marca'].'</td>
                                    <td>'.$i['cantidadVendidas'].'</td>

                                </tr>';
                        }
                    }
                ?>
            </tbody>
        </table>
        </div>
    </div>
</div>


    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>


    </body>
</html>
