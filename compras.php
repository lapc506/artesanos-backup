<?php session_start();?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>ArtesanosDB</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootsrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!-- Owl carousel -->
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Template main Css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2.min.js"></script>

    </head>
    <body>
    <?php include('header.php'); ?>

    <div class="page-heading text-center">

		<div class="container zoomIn animated">

			<h1 class="page-title">Compras <span class="title-under"></span></h1>
			<p class="page-description">
				Las mejores cervezas del país!
			</p>

		</div>

	</div>



<div class="container-fluid">
        <h2 class="title-style-2"><b>Compras</b><span class="title-under"></span></h2>
<form action="compras.php" method="get">
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <fieldset class="form-group">
            <label style="display: block;" >Nombre:</label><br>
            <input id="bnombre" name="bnombre" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Primer Apellido:</label><br>
            <input id="bapellido1" name="bapellido1" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Segundo Apellido:</label><br>
            <input id="bapellido2" name="bapellido2" type="text" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <label style="display: block;" >Cédula:</label><br>
            <input id="bcedula" name="bcedula" type="text"maxlength="10" class="form-control"/>
        </fieldset>
        <fieldset class="form-group">
            <input id="buscar" name="buscar" type="submit" value="Buscar" class="btn btn-primary btn-block"/>
        </fieldset>
    </div>
    <div class="col-md-6 col-sm-6 fadeIn animated col-form">
        <div class="panel-primary">
            <div class="panel-heading"><h5 style="display: block;" >Lista de clientes (Usuario)</h5></div>
            <div class="panel-body" style="min-height:375px;max-height:375px;overflow-y:scroll;">
                <table data-toggle="table" id="table-style" data-row-style="rowStyle"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-single-select="true" data-click-to-select="true" data-maintain-selected="true" class="table table-striped table-hover table-responsive">
                  <thead>
                    <tr>
                      <th>Usuario</th>
                      <th>Nombre</th>
                      <th>Primer Apellido</th>
                      <th>Segundo Apellido</th>
                      <th>Cédula</th>
                    </tr>
                  </thead>
                    <tbody>
                        <?php
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getClientes();');
                $result->execute();
                foreach($result as $i){
                    echo '
                        <tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&!empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getNombrelike(:nombre);');
                $result->execute(array(':nombre'=>$_GET['bnombre']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&!empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getPrimerApellidolike(:apellido1);');
                $result->execute(array(':apellido1'=>$_GET['bapellido1']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&!empty($_GET['bapellido2'])
               &&empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getSegundoApellidolike(:apellido2);');
                $result->execute(array(':apellido2'=>$_GET['bapellido2']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }
            if(!empty($_GET['buscar'])
               &&empty($_GET['bnombre'])
               &&empty($_GET['bapellido1'])
               &&empty($_GET['bapellido2'])
               &&!empty($_GET['bcedula']))
            {
                $db = new PDO(HOST,USER,PASS);
                $result = $db->prepare('call getCedulalike(:cedula);');
                $result->execute(array(':cedula'=>$_GET['bcedula']));
                foreach($result as $i){
                    echo '<tr>
                            <td><input type="radio" name="listaUsuarios" id="listaUsuarios" value="'.$i['idPersona'].'">'.$i['usuario'].'</td>
                            <td>'.$i['nombre'].'</td>
                            <td>'.$i['primerApellido'].'</td>
                            <td>'.$i['segundoApellido'].'</td>
                            <td>'.$i['numeroCedula'].'</td>
                        </tr>';
                }
            }?>



                    </tbody>
                </table>
            </div>
            <input id="REcomprar" name="REcomprar" type="submit" value="Realizar Compra"  class="btn btn-primary btn-block"/>
        </div>
    </div>
</form>
</div>
<div class="container-fluid">
        <form action="compras.php" method="post" enctype="multipart/form-data">
            <div class="fadeIn animated">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title-style-2"><b>Compra de cerveza</b><span class="title-under"></span></h2>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <label style="display: block;" >Familia de cerveza:</label>
                            <select name='bfamiliaCerveza' id="bfamiliaCerveza" class="form-control">
                                <option value="none" >Seleccione un la familia</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getFamilias();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Tipo de cerveza:</label>
                            <select name='btipoCerveza' id="btipoCerveza" class="form-control">
                                <option value="none" >Seleccione un tipo</option>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Color:</label>
                            <select name='bcolor' id="bcolor" class="form-control">
                                <option value="none" >Seleccione un color</option>
                                <?php
                                    $db = new PDO(HOST,USER,PASS);
                                    $result = $db->prepare('call getColores();');
                                    $result->execute();
                                    foreach($result as $i){
                                        echo '<option value='.$i['id'].'>'.$i['descripcion'].'</option>';
                                    }?>
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label style="display: block;" >Marca:</label>
                            <input id="bmarca" name="bmarca" type="text" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <input id="buscar" name="buscar" value="Buscar"type="submit" class="btn btn-primary btn-block"/>
                        </fieldset>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <select id="listaCervezas" name="listaCervezas" type="listbox" size="12" class="form-control">
                           <?php // Check if image file is a actual image or fake image
                            if(isset($_POST["buscar"])) {
                                $db = new PDO(HOST,USER,PASS);
                                if($_POST['btipoCerveza']!='none'&&
                                   $_POST['bmarca']==''&&
                                   $_POST['bcolor']=='none'){
                                    $stmt = $db ->prepare('call getTipoCervezaById(:idTipo)');
                                    $stmt -> execute(array(':idTipo'=>$_POST['btipoCerveza']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }

                                if($_POST['btipoCerveza']=='none'&&
                                   $_POST['bmarca']==''&&
                                   $_POST['bcolor']!='none'){
                                    $stmt = $db ->prepare('call getColorById(:idColor)');
                                    $stmt -> execute(array(':idColor'=>$_POST['bcolor']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }
                                if($_POST['btipoCerveza']=='none'&&
                                   $_POST['bmarca']!=''&&
                                   $_POST['bcolor']=='none'){
                                    $stmt = $db ->prepare('call getCervezaByMarca(:marca)');
                                    $stmt -> execute(array(':marca'=>$_POST['bmarca']));
                                    foreach($stmt as $i){
                                    echo '<option value='.$i['id'].'>'.$i['marca'].'</option>';
                                }
                                }
                            }
                            ?>
                           </select>
                        </fieldset>
                    </div>
                </div>
            </div>



            <div class="fadeIn animated">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <h2 class="title-style-2"><b>Cantidad</b><span class="title-under"></span></h2>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="form-group">
                            <label style="display: block; " >Cantidad de Cervezas</label>
                            <input id="cant" name="cant" min="1"  type="number" class="form-control"/>
                        </fieldset>
                        <fieldset class="form-group">
                            <input id="compra" name="compra" value ="Registrar Compra"type="submit" class="btn btn-primary btn-block"/>
                        </fieldset>

                    </div>
                </div>
            </div>
    </form>
<?php
        if(isset($_GET['REcomprar'])){
            $_SESSION['comprador']=$_GET['listaUsuarios'];

        }



        if(isset($_POST['compra'])){
            if(!empty($_SESSION['comprador'])){
            $db = new PDO(HOST,USER,PASS);
            $stmt = $db ->prepare("call registrarCompra(:idPersona,:idCerveza,:cantidad);");
            $stmt -> execute(array(':idPersona'=> $_SESSION['comprador'],
                               ':idCerveza'=> $_POST['listaCervezas'],
                               ':cantidad'=> $_POST['cant']));

            echo '<script>alert("Compra Registrada!");</script>';
            }else{echo  '<script>alert("Seleccione un cliente primero");</script>';}}




        ?>



    <!--  Scripts
    ================================================== -->

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

    <!-- Bootsrap javascript file -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- owl carouseljavascript file -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Template main javascript -->
    <script src="assets/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
<script>$(document).ready(function () {
  //called when key is pressed in textbox
        $("#precio").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    });



    $('#bfamiliaCerveza').change(function(){
        var listFamilia = document.getElementById("bfamiliaCerveza")
        var Familia = listFamilia.options[listFamilia.selectedIndex].value;
        var select = document.getElementById("btipoCerveza");
        //var select2 = document.getElementById("distrito");
        var o = document.createElement("OPTION");

        o.text="Seleccione un tipo";
        o.value="none";
        select.length = 0;
        select.options.add(o);

        <?php
            $db = new PDO(HOST,USER,PASS);
            $t = $db->prepare('call getTipoCerveza();');
            $t->execute();
            foreach($t as $type){?>
                if (Familia == <?php echo $type['idFamilia'];?>){
                    o = document.createElement("OPTION");
                    o.text = <?php echo "'".$type["descripcion"]."'";?>;
				    o.value = <?php echo $type["id"];?>;
				    select.options.add (o);

                    }
                <?php } ?>

    });
</script>

    </body>
</html>
